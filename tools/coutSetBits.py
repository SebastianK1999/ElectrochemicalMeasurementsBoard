def  countSetBits(n):
    count = 0
    while (n):
        count += n & 1
        n >>= 1
    return count

var_type = "int"
var_name = "arr"
debug_mode = "bits" # bits, hex, none

print(var_type + " " + var_name + "[] = \n{\n    ", end="")
for i in range(0,256):
    if(debug_mode == "none"):
        print(countSetBits(i), end=",")
        if((i+1)%4 == 0):
            print(" ", end="")
        if((i+1)%16 == 0):
            print("\n    ", end="")
    else:
        if debug_mode == "hex":    
            print("(", i,":", hex(i), ":",countSetBits(i), ")")
        elif debug_mode == "bits":    
            print("(", i,":", "{0:b}".format(i), ":",countSetBits(i), ")")

print("\r}")