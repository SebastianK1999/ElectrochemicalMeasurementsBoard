Quick start
-----------

Install necessary tools::

  $ pip3 install --user west ninja cmake gitlint

Setup project::

  $ mkdir uart-example
  $ cd uart-example
  $ west init -m git@gitlab.com:mucka_private/nucleo-f411re-uart-example.git
  $ west update

Build yoli nrf firmware::

  $ . zephyr/zephyr-env.sh
  $ west build -b nucleo_f411re app
