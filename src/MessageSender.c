#include "MessageSender.h"

#include <logging/log.h>
#include "impedanceutilitieslib/include/Utilities.h"
#include "UartIO.h"
#include "BoardConfiguration.h"
LOG_MODULE_REGISTER(MessageSender, LOG_LEVEL_DBG);

void sendMessage(enum EBoardToPcMessage messageType, const void * data, const size_t length)
{
    #define MAX_BUFFER_SIZE    12
    static uint8_t bufferIndex = 0;
    static MessageByteType buffers[MAX_BUFFER_SIZE][HEAD_SIZE + MAX_MESSAGE_SIZE];
    while(k_msgq_num_used_get(&tx_msgq) >= MAX_BUFFER_SIZE){
        k_msleep(10);
    }
    MessageByteType* buffer = buffers[bufferIndex];
    MessageByteType* bufferAt = (buffer + 0);
    memcpy(bufferAt, kPreamble, sizeof(kPreamble));
    bufferAt = (buffer + MESSAGE_TYPE_INDEX);
    *(SerializedMessageTypeType*)bufferAt = (SerializedMessageTypeType)messageType;
    bufferAt = (buffer + MESSAGE_DATA_INDEX);
    memcpy(bufferAt, data, length);
    bufferAt = (buffer + VALIDATORS_SIZE);
    ControlSumType controlSum = calculateControlSum(bufferAt,(HEAD_SIZE - VALIDATORS_SIZE) + length);
    bufferAt = (buffer + CONTROL_SUM_INDEX);
    *(ControlSumType*)bufferAt = controlSum;
    //LOG_HEXDUMP_DBG(buffer, HEAD_SIZE + length, "sendMessage");
    uart_send_async(buffer, HEAD_SIZE + length);  
    bufferIndex++;
    if(bufferIndex == MAX_BUFFER_SIZE)
    {
        bufferIndex = 0; 
    }
}

