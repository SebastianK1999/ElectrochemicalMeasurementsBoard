#define _GNU_SOURCE
double sqrt(double);

#include <stdbool.h>
#include <logging/log.h>
LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);
#include "Ad5940LibOverrides.h"
#include "ad5940lib/ad5940.h"
#include "impedanceutilitieslib/include/Configuration.h"
#include "UartIO.h"
#include "AppConfiguration.h"	
#include "EISApp.h"
#include "CVApp.h"
#include "Commands.h"
#include "BoardConfiguration.h"

void main(void)
{
	LOG_INF("Entering main thread ...");
	LOG_INF("Version compiled at %s [%s]", __DATE__, __TIME__);

	configMCUInteruptFlag();
	init_uart();
	k_msleep(1000);

	uint32_t analogDeviceId = AD5940_ReadReg(0x00000400);
	LOG_INF("Analog Device id: %d", analogDeviceId);
	uint32_t chipId = AD5940_GetChipID();
	LOG_INF("Chip id: %d", chipId);

    // struct AppParameters* appParameters = getAppParametersPtr();
	// appParameters->appIsRunning = 1;
	// appParameters->rampConfig.rampBothDirections = 1;
    // appParameters->rampConfig.rampStartMiliVolt =  -1000.0f;           /* -1V */
    // appParameters->rampConfig.rampPeakMiliVolt = +1000.0f;           /* +1V */
    // appParameters->rampConfig.rampVzeroStart = 1300.0f;               /* 1.3V */
    // appParameters->rampConfig.rampVzeroPeak = 1300.0f;                /* 1.3V */
    // appParameters->rampConfig.rampStepNumber = 512;                   /* Total steps. Equals to ADC sample number */
    // appParameters->rampConfig.rampDuration = 24*1000;            /* X * 1000, where x is total duration of ramp signal. Unit is ms. */
    // appParameters->rampConfig.rampSampleDelay = 7.0f;                 /* 7ms. Time between update DAC and ADC sample. Unit is ms. */

	// while (1)
	// {
	// 	RunCVApp();
	// 	k_msleep(1);
	// }

	LOG_INF("Leaving main thread ...");
}
