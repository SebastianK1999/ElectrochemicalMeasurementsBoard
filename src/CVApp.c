#include "CVApp.h"

#include <math.h>
#include <stdbool.h>
#include <logging/log.h>
LOG_MODULE_REGISTER(RunCVApp, LOG_LEVEL_DBG);
#include "Ad5940LibOverrides.h"
#include "ad5940lib/ad5940.h"
#include "impedanceutilitieslib/include/Configuration.h"
#include "AppConfiguration.h"
#include "UartIO.h"
#include "BoardConfiguration.h"
#include "MessageSender.h"
#include "mathDeclarations.h"

#define ALIGIN_VOLT2LSB     0   /* Set it to 1 to align each voltage step to 1LSB of DAC. 0: step code is fractional. */
#define DAC12BITVOLT_1LSB   (2200.0f/4095)  //mV
#define DAC6BITVOLT_1LSB    (DAC12BITVOLT_1LSB*64)  //mV

static void configCVAppSystemParametersSequencer()
{
    LOG_INF("Configuring sequence parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    appParameters->seqencerConfig.SeqEnable = bFALSE;
    appParameters->seqencerConfig.SeqBreakEn = bFALSE;
    appParameters->seqencerConfig.SeqIgnoreEn = bTRUE;
    appParameters->seqencerConfig.SeqCntCRCClr = bTRUE;
    appParameters->seqencerConfig.SeqMemSize = SEQMEMSIZE_4KB;
    appParameters->seqencerConfig.SeqWrTimer = 0; 
    AD5940_SEQCfg(&(appParameters->seqencerConfig));
}

static void configCVAppSystemParametersClock()
{
    LOG_INF("Configuring clock parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    appParameters->clockConfig.HFOSCEn = bTRUE;
    appParameters->clockConfig.HFXTALEn = bFALSE;
    appParameters->clockConfig.LFOSCEn = bTRUE;
    appParameters->clockConfig.HfOSC32MHzMode = bFALSE;
    appParameters->clockConfig.ADCClkDiv = ADCCLKDIV_1;
    appParameters->clockConfig.ADCCLkSrc = ADCCLKSRC_HFOSC;
    appParameters->clockConfig.SysClkDiv = SYSCLKDIV_1;
    appParameters->clockConfig.SysClkSrc = SYSCLKSRC_HFOSC;
    AD5940_CLKCfg(&(appParameters->clockConfig));
}

static void configCVAppSystemParametersFifo()
{
    LOG_INF("Configuring fifo parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    //AD5940_FIFOCtrlS(FIFOSRC_DFT, bFALSE);
    appParameters->fifoConfig.FIFOEn = bTRUE;
    appParameters->fifoConfig.FIFOMode = FIFOMODE_FIFO;
    appParameters->fifoConfig.FIFOSize = FIFOSIZE_2KB;
    appParameters->fifoConfig.FIFOSrc = FIFOSRC_SINC3;
    appParameters->fifoConfig.FIFOThresh = 4;
    AD5940_FIFOCfg(&(appParameters->fifoConfig));
    //AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
}

static void configCVAppSystemParametersGpio()
{
    LOG_INF("Configuring gpio parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    appParameters->gpioConfig.FuncSet = GP0_INT|GP1_SLEEP|GP2_SYNC;
    appParameters->gpioConfig.InputEnSet = 0;
    appParameters->gpioConfig.OutputEnSet = AGPIO_Pin0|AGPIO_Pin1|AGPIO_Pin2;
    appParameters->gpioConfig.OutVal = 0;
    appParameters->gpioConfig.PullEnSet = 0;
    AD5940_AGPIOCfg(&(appParameters->gpioConfig));
}

static void configCVAppFrequencyMeasurementConfig()
{
    LOG_INF("Configuring frequency measurement parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    appParameters->lowFrequencyOscilatorMeasurementCongid.CalDuration = 1000.0;  /* 1000ms used for calibration. */
    appParameters->lowFrequencyOscilatorMeasurementCongid.CalSeqAddr = 0;        /* Put sequence commands from start address of SRAM */
    appParameters->lowFrequencyOscilatorMeasurementCongid.SystemClkFreq = 16000000.0f; /* 16MHz in this firmware. */
    AD5940_LFOSCMeasure(&(appParameters->lowFrequencyOscilatorMeasurementCongid), &(appParameters->LFOSCClkFreq));
}

static void configCVAppIteruptConfiguration()
{
    LOG_INF("Configuring interupts");
    AD5940_INTCCfg(AFEINTC_1, AFEINTSRC_ALLINT, bTRUE);   /* Enable all interrupt in INTC1, so we can check INTC flags */
    AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
    AD5940_INTCCfg(AFEINTC_0, AFEINTSRC_DATAFIFOTHRESH|AFEINTSRC_ENDSEQ|AFEINTSRC_CUSTOMINT0, bTRUE); 
    AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
}

static AD5940Err configCVAppRtia()
{
    LOG_INF("Configuring Rtia");
    struct AppParameters* appParameters = getAppParametersPtr();
    fImpPol_Type RtiaCalValue;  /* Calibration result */
    LPRTIACal_Type lprtia_cal;
    AD5940_StructInit(&lprtia_cal, sizeof(lprtia_cal));

    lprtia_cal.LpAmpSel = LPAMP0;
    lprtia_cal.bPolarResult = bTRUE;                /* Magnitude + Phase */
    lprtia_cal.AdcClkFreq = appParameters->AdcClkFreq;
    lprtia_cal.SysClkFreq = appParameters->SysClkFreq;
    lprtia_cal.ADCSinc3Osr = ADCSINC3OSR_4;
    lprtia_cal.ADCSinc2Osr = ADCSINC2OSR_22;        /* Use SINC2 data as DFT data source */
    lprtia_cal.DftCfg.DftNum = DFTNUM_2048;         /* Maximum DFT number */
    lprtia_cal.DftCfg.DftSrc = DFTSRC_SINC2NOTCH;
    lprtia_cal.DftCfg.HanWinEn = bTRUE;
    lprtia_cal.fFreq = appParameters->AdcClkFreq / 4 / 22 / 2048 * 3; /* Sample 3 period of signal, 13.317Hz here. Do not use DC method, because it needs ADC/PGA calibrated firstly(but it's faster) */
    lprtia_cal.fRcal = appParameters->RcalVal;
    lprtia_cal.LpTiaRtia = appParameters->LPTIARtiaSel;
    lprtia_cal.LpAmpPwrMod = LPAMPPWR_BOOST3;
    lprtia_cal.bWithCtia = bFALSE;
    AD5940_LPRtiaCal(&lprtia_cal, &RtiaCalValue);
    RtiaCalValue.Magnitude = fabs(RtiaCalValue.Magnitude);
    appParameters->RtiaValue = RtiaCalValue;
    return AD5940ERR_OK;
}

static void configCVAppOtherParameters()
{
    LOG_INF("Configuring other parameters");
    struct AppParameters* appParameters = getAppParametersPtr();


    /* Common configurations for all kinds of Application. */
    appParameters->bParaChanged = 0;         /**< Indicate to generate sequence again. It's auto cleared by AppBIAInit */
    appParameters->SeqStartAddr = 0;         /**< Initialaztion sequence start address in SRAM of AD5940  */
    appParameters->MaxSeqLen = 0;            /**< Limit the maximum sequence.   */
    appParameters->SeqStartAddrCal = 0;      /**< Not used for Ramp.Calibration sequence start address in SRAM of AD5940 */
    appParameters->MaxSeqLenCal = 0;         /**< Not used for Ramp. */
    appParameters->LFOSCClkFreq = 0;         /**< The clock frequency of Wakeup Timer in Hz. Typically it's 32kHz. Leave it here in case we calibrate clock in software method */
    appParameters->SysClkFreq = 0;           /**< The real frequency of system clock */
    appParameters->AdcClkFreq = 0;           /**< The real frequency of ADC clock */
    appParameters->RcalVal = 0;              /**< Rcal value in Ohm */
    appParameters->ADCRefVolt = 0;           /**< The real ADC voltage in mV. */
    appParameters->bTestFinished = 0;
    appParameters->LPTIARtiaSel = 0;         /**< Select RTIA */
    appParameters->LPTIARloadSel = 0;				/**< Select Rload */
    appParameters->ExternalRtiaValue = 0;    /**< The optional external RTIA value in Ohm. Disconnect internal RTIA to use external RTIA. When using internal RTIA, this value is ignored. */
    appParameters->AdcPgaGain = 0;           /**< PGA Gain select from GNPGA_1, GNPGA_1_5, GNPGA_2, GNPGA_4, GNPGA_9 !!! We must ensure signal is in range of +-1.5V which is limited by ADC input stage */   
    appParameters->ADCSinc3Osr = 0;          /**< We use data from SINC3 filter. */
    appParameters->FifoThresh = 0;           /**< FIFO Threshold value */
    appParameters->bFirstDACSeq = 0;     /**< Init DAC sequence */
    memset(&(appParameters->DACSeqInfo), 0, sizeof(appParameters->DACSeqInfo));
    appParameters->CurrStepPos = 0;          /**< Current position */     
    appParameters->DACCodePerStep = 0;       /**<  */
    appParameters->CurrRampCode = 0;         /**<  */   
    appParameters->CurrVzeroCode = 0;        
    appParameters->bDACCodeInc = 0;          /**< Increase DAC code.  */
    appParameters->StopRequired = 0;         /**< After FIFO is ready, stop the measurement sequence */
    appParameters->RampState = 0;







    appParameters->bParaChanged = bFALSE;
    appParameters->SeqStartAddrCal = 0;
    appParameters->MaxSeqLenCal = 0;

    appParameters->LFOSCClkFreq = 32000.0;
    appParameters->AdcClkFreq = 16000000.0;
    appParameters->bTestFinished = bFALSE;
    /* Receive path configuration */
    appParameters->ExternalRtiaValue = 20000.0f;      /* Optional external RTIA resistore value in Ohm. */
    appParameters->ADCSinc3Osr = ADCSINC3OSR_2;
    /* Priviate parameters */
    appParameters->StopRequired = bFALSE;
    appParameters->RampState = RAMP_STATE0;
    appParameters->bFirstDACSeq = bTRUE;
    appParameters->bTestFinished = bFALSE,

    
        

    appParameters->SeqStartAddr = 0x10;                /* leave 16 commands for LFOSC calibration.  */
    appParameters->MaxSeqLen = 1024-0x10;              /* 4kB/4 = 1024  */
    appParameters->RcalVal = 10000.0;                  /* 10kOhm RCAL */
    appParameters->ADCRefVolt = 1820.0f;               /* The real ADC reference voltage. Measure it from capacitor C12 with DMM. */
    appParameters->FifoThresh = appParameters->rampConfig.rampStepNumber;                   /* Maximum value is 2kB/4-1 = 512-1. Set it to higher value to save power. */
    appParameters->SysClkFreq = 16000000.0f;           /* System clock is 16MHz by default */
    /* Configure ramp signal parameters */

    appParameters->LPTIARtiaSel = LPTIARTIA_10K;       /* Maximum current decides RTIA value */
	appParameters->LPTIARloadSel = LPTIARLOAD_SHORT;
	appParameters->AdcPgaGain = ADCPGA_1P5;





}

static AD5940Err generateCVAppInitialSequence()
{
    LOG_INF("Generating initial sequence");
    struct AppParameters* appParameters = getAppParametersPtr();
    AD5940Err error = AD5940ERR_OK;
    const uint32_t *pSeqCmd;
    uint32_t SeqLen;
    AFERefCfg_Type afeReferenceConfig;
    LPLoopCfg_Type lowPowerLoopConfig;
    DSPCfg_Type digitalSignalProcessorConfig;
    // Start sequence generator here
    AD5940_SEQGenCtrl(bTRUE);
    AD5940_AFECtrlS(AFECTRL_ALL, bFALSE);

    // Configure AFE reference
    afeReferenceConfig.HpBandgapEn = bTRUE;
    afeReferenceConfig.Hp1V1BuffEn = bTRUE;
    afeReferenceConfig.Hp1V8BuffEn = bTRUE;
    afeReferenceConfig.Disc1V1Cap = bFALSE;
    afeReferenceConfig.Disc1V8Cap = bFALSE;
    afeReferenceConfig.Hp1V8ThemBuff = bFALSE;
    afeReferenceConfig.Hp1V8Ilimit = bFALSE;
    afeReferenceConfig.Lp1V1BuffEn = bFALSE;
    afeReferenceConfig.Lp1V8BuffEn = bFALSE;
    /* LP reference control - turn off them to save power*/
    afeReferenceConfig.LpBandgapEn = bTRUE;
    afeReferenceConfig.LpRefBufEn = bTRUE;
    afeReferenceConfig.LpRefBoostEn = bFALSE;
    AD5940_REFCfgS(&afeReferenceConfig);
    
    // Configure low power loop
    // // Low power DAC
    lowPowerLoopConfig.LpDacCfg.LpdacSel = LPDAC0;
    lowPowerLoopConfig.LpDacCfg.DacData12Bit = 0x800;
    lowPowerLoopConfig.LpDacCfg.DacData6Bit = 0;
    lowPowerLoopConfig.LpDacCfg.DataRst = bFALSE;
    lowPowerLoopConfig.LpDacCfg.LpDacSW = LPDACSW_VBIAS2LPPA/*|LPDACSW_VBIAS2PIN*/ | LPDACSW_VZERO2LPTIA/*|LPDACSW_VZERO2PIN*/;
    lowPowerLoopConfig.LpDacCfg.LpDacRef = LPDACREF_2P5;
    lowPowerLoopConfig.LpDacCfg.LpDacSrc = LPDACSRC_MMR;
    lowPowerLoopConfig.LpDacCfg.LpDacVbiasMux = LPDACVBIAS_12BIT; /* Step Vbias. Use 12bit DAC ouput */
    lowPowerLoopConfig.LpDacCfg.LpDacVzeroMux = LPDACVZERO_6BIT;  /* Base is Vzero. Use 6 bit DAC ouput */
    lowPowerLoopConfig.LpDacCfg.PowerEn = bTRUE;
    // // Low power AMP
    lowPowerLoopConfig.LpAmpCfg.LpAmpSel = LPAMP0;
    lowPowerLoopConfig.LpAmpCfg.LpAmpPwrMod = LPAMPPWR_BOOST3;
    lowPowerLoopConfig.LpAmpCfg.LpPaPwrEn = bTRUE;
    lowPowerLoopConfig.LpAmpCfg.LpTiaPwrEn = bTRUE;
    lowPowerLoopConfig.LpAmpCfg.LpTiaRf = LPTIARF_20K;
    lowPowerLoopConfig.LpAmpCfg.LpTiaRload = appParameters->LPTIARloadSel;
    lowPowerLoopConfig.LpAmpCfg.LpTiaRtia = appParameters->LPTIARtiaSel;
    lowPowerLoopConfig.LpAmpCfg.LpTiaSW = LPTIASW(2)|LPTIASW(4)|LPTIASW(5);
    if(appParameters->LPTIARtiaSel == LPTIARTIA_OPEN) /* User want to use external RTIA */
    {
        lowPowerLoopConfig.LpAmpCfg.LpTiaSW |= LPTIASW(9);/*|LPTIASW(10)*/ /* SW5/9 is closed to support external RTIA resistor */
    }

    AD5940_LPLoopCfgS(&lowPowerLoopConfig);

    // Configure digital signal processor
    // // ADC base 
    AD5940_StructInit(&digitalSignalProcessorConfig, sizeof(digitalSignalProcessorConfig));
    digitalSignalProcessorConfig.ADCBaseCfg.ADCMuxN = ADCMUXN_LPTIA0_N;
    digitalSignalProcessorConfig.ADCBaseCfg.ADCMuxP = ADCMUXP_LPTIA0_P;
    digitalSignalProcessorConfig.ADCBaseCfg.ADCPga = appParameters->AdcPgaGain;
    // // ADC filter
    digitalSignalProcessorConfig.ADCFilterCfg.ADCSinc3Osr = appParameters->ADCSinc3Osr;
    digitalSignalProcessorConfig.ADCFilterCfg.ADCRate = ADCRATE_800KHZ;  /* ADC runs at 16MHz clock in this example, sample rate is 800kHz */
    digitalSignalProcessorConfig.ADCFilterCfg.BpSinc3 = bFALSE;        /* We use data from SINC3 filter */
    digitalSignalProcessorConfig.ADCFilterCfg.Sinc2NotchEnable = bTRUE;
    digitalSignalProcessorConfig.ADCFilterCfg.BpNotch = bTRUE;
    digitalSignalProcessorConfig.ADCFilterCfg.ADCSinc2Osr = ADCSINC2OSR_1067;  /* Don't care */
    digitalSignalProcessorConfig.ADCFilterCfg.ADCAvgNum = ADCAVGNUM_2;   /* Don't care because it's disabled */
    AD5940_DSPCfgS(&digitalSignalProcessorConfig);

    /* Sequence end. */
    AD5940_SEQGenInsert(SEQ_STOP()); /* Add one extra command to disable sequencer for initialization sequence because we only want it to run one time. */

    /* Stop sequence generator here */
    AD5940_SEQGenCtrl(bFALSE); /* Stop sequencer generator */
    error = AD5940_SEQGenFetchSeq(&pSeqCmd, &SeqLen);
    if(error == AD5940ERR_OK)
    {
        AD5940_StructInit(&(appParameters->InitSeqInfo), sizeof(appParameters->InitSeqInfo));
        if(SeqLen >= appParameters->MaxSeqLen)
        {
            return AD5940ERR_SEQLEN;
        }
        appParameters->InitSeqInfo.SeqId = SEQID_3;
        appParameters->InitSeqInfo.SeqRamAddr = appParameters->SeqStartAddr;
        appParameters->InitSeqInfo.pSeqCmd = pSeqCmd;
        appParameters->InitSeqInfo.SeqLen = SeqLen;
        appParameters->InitSeqInfo.WriteSRAM = bTRUE;
        AD5940_SEQInfoCfg(&(appParameters->InitSeqInfo));
    }
    return error;
}

static AD5940Err generateCVAppADCSequence()
{
    LOG_INF("Generating ADC sequence");
    struct AppParameters* appParameters = getAppParametersPtr();
    AD5940Err error = AD5940ERR_OK;
    const uint32_t *pSeqCmd;
    uint32_t SeqLen;

    uint32_t WaitClks;
    ClksCalInfo_Type clks_cal;

    clks_cal.DataCount = 1; /* Sample one point everytime */
    clks_cal.DataType = DATATYPE_SINC3;
    clks_cal.ADCSinc3Osr = appParameters->ADCSinc3Osr;
    clks_cal.ADCSinc2Osr = ADCSINC2OSR_1067;  /* Don't care */
    clks_cal.ADCAvgNum = ADCAVGNUM_2; /* Don't care */
    clks_cal.RatioSys2AdcClk = appParameters->SysClkFreq / appParameters->AdcClkFreq;
    AD5940_ClksCalculate(&clks_cal, &WaitClks);

    AD5940_SEQGenCtrl(bTRUE);
    AD5940_SEQGpioCtrlS(AGPIO_Pin2);
    AD5940_AFECtrlS(AFECTRL_ADCPWR, bTRUE);
    AD5940_SEQGenInsert(SEQ_WAIT(16 * 250)); /* wait 250us for reference power up */
    AD5940_AFECtrlS(AFECTRL_ADCCNV, bTRUE);  /* Start ADC convert and DFT */
    AD5940_SEQGenInsert(SEQ_WAIT(WaitClks));  /* wait for first data ready */
    AD5940_AFECtrlS(AFECTRL_ADCPWR | AFECTRL_ADCCNV, bFALSE); /* Stop ADC */
    AD5940_SEQGpioCtrlS(0);
    AD5940_EnterSleepS();/* Goto hibernate */
    /* Sequence end. */
    error = AD5940_SEQGenFetchSeq(&pSeqCmd, &SeqLen);
    AD5940_SEQGenCtrl(bFALSE); /* Stop sequencer generator */

    if(error == AD5940ERR_OK)
    {
        AD5940_StructInit(&(appParameters->ADCSeqInfo), sizeof(appParameters->ADCSeqInfo));
        if((SeqLen + appParameters->InitSeqInfo.SeqLen) >= appParameters->MaxSeqLen)
        {
            return AD5940ERR_SEQLEN;
        }
        appParameters->ADCSeqInfo.SeqId = SEQID_2;
        appParameters->ADCSeqInfo.SeqRamAddr = appParameters->InitSeqInfo.SeqRamAddr + appParameters->InitSeqInfo.SeqLen ;
        appParameters->ADCSeqInfo.pSeqCmd = pSeqCmd;
        appParameters->ADCSeqInfo.SeqLen = SeqLen;
        appParameters->ADCSeqInfo.WriteSRAM = bTRUE;
        AD5940_SEQInfoCfg(&(appParameters->ADCSeqInfo));
    }
    return error;
}

static AD5940Err RampDacRegUpdate(uint32_t *pDACData)
    {
    struct AppParameters* appParameters = getAppParametersPtr();
    uint32_t VbiasCode;
    uint32_t VzeroCode;

    if (appParameters->rampConfig.rampBothDirections)
    {
        switch(appParameters->RampState)
        {
        case RAMP_STATE0: /* Begin of Ramp  */
            appParameters->CurrVzeroCode = (uint32_t)((appParameters->rampConfig.rampVzeroStart - 200.0f) / DAC6BITVOLT_1LSB);
            appParameters->RampState = RAMP_STATE1;
            break;
        case RAMP_STATE1:
            if(appParameters->CurrStepPos >= appParameters->rampConfig.rampStepNumber / 4)
                {
                appParameters->RampState = RAMP_STATE2;   /* Enter State2 */
                appParameters->CurrVzeroCode = (uint32_t)((appParameters->rampConfig.rampVzeroPeak - 200.0f) / DAC6BITVOLT_1LSB);
                }
            break;
        case RAMP_STATE2:
            if(appParameters->CurrStepPos >= (appParameters->rampConfig.rampStepNumber * 2) / 4)
                {
                appParameters->RampState = RAMP_STATE3;   /* Enter State3 */
                appParameters->bDACCodeInc = appParameters->bDACCodeInc ? bFALSE : bTRUE;

                }
            break;
        case RAMP_STATE3:
            if(appParameters->CurrStepPos >= (appParameters->rampConfig.rampStepNumber * 3) / 4)
                {
                appParameters->RampState = RAMP_STATE4;   /* Enter State4 */
                appParameters->CurrVzeroCode = (uint32_t)((appParameters->rampConfig.rampVzeroStart - 200.0f) / DAC6BITVOLT_1LSB);
                }
            break;
        case RAMP_STATE4:
            if(appParameters->CurrStepPos >= appParameters->rampConfig.rampStepNumber)
                appParameters->RampState = RAMP_STOP;     /* Enter Stop */
            break;
        case RAMP_STOP:
            break;
        }
    }
    else
    {
        switch(appParameters->RampState)
        {
        case RAMP_STATE0: /* Begin of Ramp  */
            appParameters->CurrVzeroCode = (uint32_t)((appParameters->rampConfig.rampVzeroStart - 200.0f) / DAC6BITVOLT_1LSB);
            appParameters->RampState = RAMP_STATE1;
            break;
        case RAMP_STATE1: 
            if(appParameters->CurrStepPos >= appParameters->rampConfig.rampStepNumber / 2)
                {
                appParameters->RampState = RAMP_STATE4;   /* Enter State4 */
                appParameters->CurrVzeroCode = (uint32_t)((appParameters->rampConfig.rampVzeroPeak - 200.0f) / DAC6BITVOLT_1LSB);
                }
            break;
        case RAMP_STATE4:
            if(appParameters->CurrStepPos >= appParameters->rampConfig.rampStepNumber)
                appParameters->RampState = RAMP_STOP;     /* Enter Stop */
            break;
        case RAMP_STOP:
            break;
        default:
            break;
        }
    }

    appParameters->CurrStepPos ++;
    if(appParameters->bDACCodeInc)
        appParameters->CurrRampCode += appParameters->DACCodePerStep;
    else
        appParameters->CurrRampCode -= appParameters->DACCodePerStep;
    VzeroCode = appParameters->CurrVzeroCode;
    VbiasCode = (uint32_t)(VzeroCode * 64 + appParameters->CurrRampCode);
    if(VbiasCode < (VzeroCode * 64))
        VbiasCode --;
    /* Truncate */
    if(VbiasCode > 4095) VbiasCode = 4095;
    if(VzeroCode >   63) VzeroCode =   63;
    *pDACData = (VzeroCode << 12) | VbiasCode;
    return AD5940ERR_OK;
}

static AD5940Err generateCVAppDACSequence()
{
    LOG_INF("Generating DAC sequence");
    #define SEQLEN_ONESTEP    4L  /* How many sequence commands are needed to update LPDAC. */
    #define CURRBLK_BLK0      0   /* Current block is BLOCK0 */
    #define CURRBLK_BLK1      1   /* Current block is BLOCK1 */
    struct AppParameters* appParameters = getAppParametersPtr();
    AD5940Err error = AD5940ERR_OK;
    uint32_t BlockStartSRAMAddr;
    uint32_t DACData, SRAMAddr;
    uint32_t i;
    uint32_t StepsThisBlock;
    BoolFlag bIsFinalBlk;
    uint32_t SeqCmdBuff[SEQLEN_ONESTEP];

    /* All below static variables are inited in below 'if' block. They are only used in this function */
    static BoolFlag bCmdForSeq0 = bTRUE;
    static uint32_t DACSeqBlk0Addr, DACSeqBlk1Addr;
    static uint32_t StepsRemainning, StepsPerBlock, DACSeqCurrBlk;

    /* Do some math calculations */
    if(appParameters->bFirstDACSeq == bTRUE)
    {
        /* Reset bIsFirstRun at end of function. */
        int32_t DACSeqLenMax;
        StepsRemainning = appParameters->rampConfig.rampStepNumber;
        DACSeqLenMax = (int32_t)appParameters->MaxSeqLen - (int32_t)appParameters->InitSeqInfo.SeqLen - (int32_t)appParameters->ADCSeqInfo.SeqLen;
        if(DACSeqLenMax < SEQLEN_ONESTEP * 4)
        {
            return AD5940ERR_SEQLEN;  /* No enough sequencer SRAM available */
        }
        DACSeqLenMax -= SEQLEN_ONESTEP * 2; /* Reserve commands each block */
        StepsPerBlock = DACSeqLenMax / SEQLEN_ONESTEP / 2;
        DACSeqBlk0Addr = appParameters->ADCSeqInfo.SeqRamAddr + appParameters->ADCSeqInfo.SeqLen;
        DACSeqBlk1Addr = DACSeqBlk0Addr + StepsPerBlock * SEQLEN_ONESTEP;
        DACSeqCurrBlk = CURRBLK_BLK0;

        /* Analog part */
        if (appParameters->rampConfig.rampBothDirections)
        {
            /* Ramping between rampConfig.rampStartMiliVolt and rampConfig.rampPeakMiliVolt in rampConfig.rampStepNumber/2 steps  */  
            appParameters->DACCodePerStep = ((appParameters->rampConfig.rampPeakMiliVolt - appParameters->rampConfig.rampStartMiliVolt) / appParameters->rampConfig.rampStepNumber * 2)
                                        / DAC12BITVOLT_1LSB;
        }
        else
        {
            /* Ramping between rampConfig.rampStartMiliVolt and rampConfig.rampPeakMiliVolt in rampConfig.rampStepNumber steps  */
            appParameters->DACCodePerStep = ((appParameters->rampConfig.rampPeakMiliVolt - appParameters->rampConfig.rampStartMiliVolt) / appParameters->rampConfig.rampStepNumber)
                                        / DAC12BITVOLT_1LSB;
        }   

#if ALIGIN_VOLT2LSB
        appParameters->DACCodePerStep = (int32_t)appParameters->DACCodePerStep;
#endif
        if(appParameters->DACCodePerStep > 0)
        {
            appParameters->bDACCodeInc = bTRUE;
        }
        else
        {
            appParameters->DACCodePerStep = -appParameters->DACCodePerStep; /* Always positive */
            appParameters->bDACCodeInc = bFALSE;
        }
        appParameters->CurrRampCode = appParameters->rampConfig.rampStartMiliVolt / DAC12BITVOLT_1LSB;

        appParameters->RampState = RAMP_STATE0;   /* Init state to STATE0 */
        appParameters->CurrStepPos = 0;

        bCmdForSeq0 = bTRUE;      /* Start with SEQ0 */
    }

    if(StepsRemainning == 0)
    {
        return AD5940ERR_OK; /* Done. */
    }
    bIsFinalBlk = StepsRemainning <= StepsPerBlock ? bTRUE : bFALSE;
    if(bIsFinalBlk)
    {
        StepsThisBlock = StepsRemainning;
    }
    else
    {
        StepsThisBlock = StepsPerBlock;
    }
    StepsRemainning -= StepsThisBlock;

    BlockStartSRAMAddr = (DACSeqCurrBlk == CURRBLK_BLK0) ? \
                         DACSeqBlk0Addr : DACSeqBlk1Addr;
    SRAMAddr = BlockStartSRAMAddr;

    for(i = 0; i < StepsThisBlock - 1; i++)
    {
        uint32_t CurrAddr = SRAMAddr;
        SRAMAddr += SEQLEN_ONESTEP;  /* Jump to next sequence */
        RampDacRegUpdate(&DACData);
        SeqCmdBuff[0] = SEQ_WR(REG_AFE_LPDACDAT0, DACData);
        SeqCmdBuff[1] = SEQ_WAIT(10); /* !!!NOTE LPDAC need 10 clocks to update data. Before send AFE to sleep state, wait 10 extra clocks */
        SeqCmdBuff[2] = SEQ_WR(bCmdForSeq0 ? REG_AFE_SEQ1INFO : REG_AFE_SEQ0INFO, \
                               (SRAMAddr << BITP_AFE_SEQ1INFO_ADDR) | (SEQLEN_ONESTEP << BITP_AFE_SEQ1INFO_LEN));
        SeqCmdBuff[3] = SEQ_SLP();
        AD5940_SEQCmdWrite(CurrAddr, SeqCmdBuff, SEQLEN_ONESTEP);
        bCmdForSeq0 = bCmdForSeq0 ? bFALSE : bTRUE;
    }
    /* Add final DAC update */
    if(bIsFinalBlk)/* This is the final block */
    {
        uint32_t CurrAddr = SRAMAddr;
        SRAMAddr += SEQLEN_ONESTEP;  /* Jump to next sequence */
        /* After update LPDAC with final data, we let sequencer to run 'final final' command, to disable sequencer.  */
        RampDacRegUpdate(&DACData);
        SeqCmdBuff[0] = SEQ_WR(REG_AFE_LPDACDAT0, DACData);
        SeqCmdBuff[1] = SEQ_WAIT(10); /* !!!NOTE LPDAC need 10 clocks to update data. Before send AFE to sleep state, wait 10 extra clocks */
        SeqCmdBuff[2] = SEQ_WR(bCmdForSeq0 ? REG_AFE_SEQ1INFO : REG_AFE_SEQ0INFO, \
                               (SRAMAddr << BITP_AFE_SEQ1INFO_ADDR) | (SEQLEN_ONESTEP << BITP_AFE_SEQ1INFO_LEN));
        SeqCmdBuff[3] = SEQ_SLP();
        AD5940_SEQCmdWrite(CurrAddr, SeqCmdBuff, SEQLEN_ONESTEP);
        CurrAddr += SEQLEN_ONESTEP;
        /* The final final command is to disable sequencer. */
        SeqCmdBuff[0] = SEQ_NOP();    /* Do nothing */
        SeqCmdBuff[1] = SEQ_NOP();
        SeqCmdBuff[2] = SEQ_NOP();
        SeqCmdBuff[3] = SEQ_STOP();   /* Stop sequencer. */
        /* Disable sequencer, END of sequencer interrupt is generated. */
        AD5940_SEQCmdWrite(CurrAddr, SeqCmdBuff, SEQLEN_ONESTEP);
    }
    else /* This is not the final block */
    {
        /* Jump to next block. */
        uint32_t CurrAddr = SRAMAddr;
        SRAMAddr = (DACSeqCurrBlk == CURRBLK_BLK0) ? \
                   DACSeqBlk1Addr : DACSeqBlk0Addr;
        RampDacRegUpdate(&DACData);
        SeqCmdBuff[0] = SEQ_WR(REG_AFE_LPDACDAT0, DACData);
        SeqCmdBuff[1] = SEQ_WAIT(10);
        SeqCmdBuff[2] = SEQ_WR(bCmdForSeq0 ? REG_AFE_SEQ1INFO : REG_AFE_SEQ0INFO,
                               (SRAMAddr << BITP_AFE_SEQ1INFO_ADDR) | (SEQLEN_ONESTEP << BITP_AFE_SEQ1INFO_LEN));
        SeqCmdBuff[3] = SEQ_INT0(); /* Generate Custom interrupt 0. */
        AD5940_SEQCmdWrite(CurrAddr, SeqCmdBuff, SEQLEN_ONESTEP);
        bCmdForSeq0 = bCmdForSeq0 ? bFALSE : bTRUE;
    }

    DACSeqCurrBlk = (DACSeqCurrBlk == CURRBLK_BLK0) ? \
                    CURRBLK_BLK1 : CURRBLK_BLK0; /* Switch between Block0 and block1 */
    if(appParameters->bFirstDACSeq)
    {
        appParameters->bFirstDACSeq = bFALSE;
        if(bIsFinalBlk == bFALSE)
        {
            /* Otherwise there is no need to init block1 sequence */
            error = generateCVAppDACSequence();
            if(error != AD5940ERR_OK)
                return error;
        }
        /* This is the first DAC sequence. */
        appParameters->DACSeqInfo.SeqId = SEQID_0;
        appParameters->DACSeqInfo.SeqLen = SEQLEN_ONESTEP;
        appParameters->DACSeqInfo.SeqRamAddr = BlockStartSRAMAddr;
        appParameters->DACSeqInfo.WriteSRAM = bFALSE; /* No need to write to SRAM. We already write them above. */
        AD5940_SEQInfoCfg(&appParameters->DACSeqInfo);
    }
    return AD5940ERR_OK;
}

AD5940Err configCVAppInitializationSequence()
{
    LOG_INF("Initialize sequence");
    struct AppParameters* appParameters = getAppParametersPtr();

    FIFOCfg_Type fifo_cfg;
    SEQCfg_Type seq_cfg;

    if(AD5940_WakeUp(10) > 10)
    {
        return AD5940ERR_WAKEUP;
    }

    /* Configure sequencer and stop it */
    seq_cfg.SeqMemSize = SEQMEMSIZE_2KB;
    seq_cfg.SeqBreakEn = bFALSE;
    seq_cfg.SeqIgnoreEn = bFALSE;
    seq_cfg.SeqCntCRCClr = bTRUE;
    seq_cfg.SeqEnable = bFALSE;
    seq_cfg.SeqWrTimer = 0;
    AD5940_SEQCfg(&seq_cfg);

    if(appParameters->LPTIARtiaSel == LPTIARTIA_OPEN) /* Internal RTIA is opened. User wants to use external RTIA resistor */
    {
        appParameters->RtiaValue.Magnitude = appParameters->ExternalRtiaValue;
        appParameters->RtiaValue.Phase = 0;
    }
    else
    {
        for (int i = 0; i < 10; i++)
        {
            configCVAppRtia();
            LOG_HEXDUMP_DBG(&(appParameters->RtiaValue.Magnitude), 4, "rtia");
            k_msleep(10);
        }
    }
    AD5940_SEQGenInit(appsAD5940IOBuffer, APPS_AD5940_IO_BUFFER_SIZE);
    /* Generate sequence and write them to SRAM start from address appParameters->SeqStartAddr */
    AD5940Err error;
    error = generateCVAppInitialSequence();      /* Application initialization sequence */
    if(error != AD5940ERR_OK)
    {
        return error;
    }
    error = generateCVAppADCSequence();   /* ADC control sequence */
    if(error != AD5940ERR_OK)
    {
        return error;
    }

    AD5940_FIFOCtrlS(FIFOSRC_SINC3, bFALSE);        /* Disable FIFO firstly */
    fifo_cfg.FIFOEn = bTRUE;
    fifo_cfg.FIFOSrc = FIFOSRC_SINC3;
    fifo_cfg.FIFOThresh = appParameters->FifoThresh;    /* Change FIFO paramters */
    fifo_cfg.FIFOMode = FIFOMODE_FIFO;
    fifo_cfg.FIFOSize = FIFOSIZE_4KB;
    AD5940_FIFOCfg(&fifo_cfg);

    AD5940_INTCClrFlag(AFEINTSRC_ALLINT); // clear all interrupts
    appParameters->bFirstDACSeq = bTRUE; // todo ????
    error = generateCVAppDACSequence();
    if(error != AD5940ERR_OK)
    {
        return error;
    }

    /* Configure sequence info. */
    appParameters->InitSeqInfo.WriteSRAM = bFALSE;
    AD5940_SEQInfoCfg(&(appParameters->InitSeqInfo));

    AD5940_SEQCtrlS(bTRUE); /* Enable sequencer */
    AD5940_SEQMmrTrig(appParameters->InitSeqInfo.SeqId);
    while(AD5940_INTCTestFlag(AFEINTC_1, AFEINTSRC_ENDSEQ) == bFALSE)
    {
        k_msleep(1);
    }

    AD5940_INTCClrFlag(AFEINTSRC_ENDSEQ);
    appParameters->ADCSeqInfo.WriteSRAM = bFALSE;
    AD5940_SEQInfoCfg(&(appParameters->ADCSeqInfo));
    appParameters->DACSeqInfo.WriteSRAM = bFALSE;
    AD5940_SEQInfoCfg(&(appParameters->DACSeqInfo));

    AD5940_SEQCtrlS(bFALSE);
    AD5940_WriteReg(REG_AFE_SEQCNT, 0);
    AD5940_SEQCtrlS(bTRUE);   /* Enable sequencer, and wait for trigger */
    clearMCUInteruptFlag();   /* Clear interrupt flag generated before */
    AD5940_AFEPwrBW(AFEPWR_LP, AFEBW_250KHZ); /* Set to low power mode */
    return AD5940ERR_OK;

}

AD5940Err configCVAppParametersWakeUpTimer()
{
    LOG_INF("Configure wake up timer parameter");
    struct AppParameters* appParameters = getAppParametersPtr();
    if(AD5940_WakeUp(10) > 10)
    {
        return AD5940ERR_WAKEUP;
    }
    if(appParameters->RampState == RAMP_STOP)
                return AD5940ERR_APPERROR;
    appParameters->wakeUpTimerConfig.WuptEn = bTRUE;
    appParameters->wakeUpTimerConfig.WuptEndSeq = WUPTENDSEQ_D;
    appParameters->wakeUpTimerConfig.WuptOrder[0] = SEQID_0;
    appParameters->wakeUpTimerConfig.WuptOrder[1] = SEQID_2;
    appParameters->wakeUpTimerConfig.WuptOrder[2] = SEQID_1;
    appParameters->wakeUpTimerConfig.WuptOrder[3] = SEQID_2;
    appParameters->wakeUpTimerConfig.SeqxSleepTime[SEQID_2] = 4;
    appParameters->wakeUpTimerConfig.SeqxWakeupTime[SEQID_2] = (uint32_t)(appParameters->LFOSCClkFreq * appParameters->rampConfig.rampSampleDelay / 1000.0f) - 4 - 2;
    appParameters->wakeUpTimerConfig.SeqxSleepTime[SEQID_0] = 4;
    appParameters->wakeUpTimerConfig.SeqxWakeupTime[SEQID_0] = (uint32_t)(appParameters->LFOSCClkFreq * (appParameters->rampConfig.rampDuration / appParameters->rampConfig.rampStepNumber - appParameters->rampConfig.rampSampleDelay) / 1000.0f) - 4 - 2;
    appParameters->wakeUpTimerConfig.SeqxSleepTime[SEQID_1] = appParameters->wakeUpTimerConfig.SeqxSleepTime[SEQID_0];
    appParameters->wakeUpTimerConfig.SeqxWakeupTime[SEQID_1] = appParameters->wakeUpTimerConfig.SeqxWakeupTime[SEQID_0];
    AD5940_WUPTCfg(&(appParameters->wakeUpTimerConfig));
    return AD5940ERR_OK;
}

static void AppRAMPRegModify()
{

    struct AppParameters* appParameters = getAppParametersPtr();
    if(appParameters->StopRequired == bTRUE)
    {
        AD5940_WUPTCtrl(bFALSE);
    }
}

void sendCVAppMessage(float* pMeasures, const uint32_t count)
{
    LOG_INF("Sending message");

    struct AppParameters* appParameters = getAppParametersPtr();
    struct MessageBoardToPcCVMeasurement message;
    const float dVoltage = appParameters->rampConfig.rampPeakMiliVolt - appParameters->rampConfig.rampStartMiliVolt; 
    const int halveCount = count/2;
    int i = 0;
    if(appParameters->rampConfig.rampBothDirections)
    {
        const float stepVoltage = dVoltage / count * 2; 
        for(; i < halveCount; i++){
            message.mACurrent = pMeasures[i];
            message.mVVoltage = (appParameters->rampConfig.rampStartMiliVolt + stepVoltage*i);
            sendMessage(EBoardToPcMessageCVMeasurement, &message, sizeof(struct MessageBoardToPcCVMeasurement));
        }
        for(; i < count; i++){
            message.mACurrent = pMeasures[i];
            message.mVVoltage = (appParameters->rampConfig.rampPeakMiliVolt - stepVoltage*(i-halveCount));
            sendMessage(EBoardToPcMessageCVMeasurement, &message, sizeof(struct MessageBoardToPcCVMeasurement));
        }
    }
    else
    {
        const float stepVoltage = dVoltage / count; 
        for(; i < count; i++){
            message.mACurrent = pMeasures[i];
            message.mVVoltage = (appParameters->rampConfig.rampStartMiliVolt + stepVoltage*i);
            sendMessage(EBoardToPcMessageCVMeasurement, &message, sizeof(struct MessageBoardToPcCVMeasurement));
        }
    }
}

static void AppRAMPDataProcess(int32_t* const pData, uint32_t pDataCount)
{
    LOG_INF("Processing data");
    struct AppParameters* appParameters = getAppParametersPtr();
    float *pOut = (float *)pData;
    float temp;
    int32_t tempData;
    for(int i = 0; i < pDataCount; i++)
    {
        pData[i] &= 0xffff;	
        temp = -AD5940_ADCCode2Volt(pData[i], appParameters->AdcPgaGain, appParameters->ADCRefVolt);	
        pOut[i] = temp / appParameters->RtiaValue.Magnitude * 1e3f; /* Result unit is uA. */
       
        
        // LOG_INF("measure %d, %d, %d", pData[i], (int16_t) data[0], (int16_t) data[1]);
        // k_msleep(10);
        //LOG_HEXDUMP_DBG(&(pData[i]), 4, "raw data");
        // tempData = pData[i] & 0x0000ffff;
        // U0 = -AD5940_ADCCode2Volt(tempData, appParameters->AdcPgaGain, appParameters->ADCRefVolt);	
        // tempData = pData[i] >> 16;
        // U1 = -AD5940_ADCCode2Volt(tempData, appParameters->AdcPgaGain, appParameters->ADCRefVolt);	
        // I0 = U0 / appParameters->RtiaValue.Magnitude * 1e3f; /* Result unit is uA. */
        // I1 = U1 / appParameters->RtiaValue.Magnitude * 1e3f; /* Result unit is uA. */
        // LOG_INF("measure %d, %d, %d, %d, %d, %d, %d", pData[i] & 0x0000ffff, tempData, (int32_t) (U0 * 1000.0), (int32_t) (U1 * 1000.0), (int32_t) (appParameters->RtiaValue.Magnitude / 1000.0), (int32_t) (I0 * 1000000.0), (int32_t) (I1 * 1000000.0));
        // k_msleep(20);
    }
}

void RunCVApp()
{
    LOG_INF("Running CVApp");
    struct AppParameters* appParameters = getAppParametersPtr();
    AD5940Err error = 0;
    resetBoard();
    configCVAppSystemParametersClock();
    configCVAppSystemParametersFifo();
    configCVAppSystemParametersSequencer();
    configCVAppIteruptConfiguration();
    configCVAppSystemParametersGpio();
    configCVAppFrequencyMeasurementConfig();
    AD5940_SleepKeyCtrlS(SLPKEY_UNLOCK);   
    configCVAppOtherParameters();
    error = configCVAppInitializationSequence();
    error = configCVAppParametersWakeUpTimer();
    if(error != AD5940ERR_OK)
    {
        LOG_ERR("CVApp Configuration Error");
        stopSequence();
        switch (error)
        {
        case AD5940ERR_WAKEUP:
            sendStatusMessage(EBoardStatusWakeUp);
            break;
        
        default:
            sendStatusMessage(EBoardStatusBadRequest);
            break;
        }
        return;
    }

    size_t measurementsCount = 0;
    uint32_t timeoutCount = 0;
    uint32_t timeoutMax = appParameters->rampConfig.rampDuration + CVAPP_ADD_TIMEOUT_MILISECONDS;
    while(appParameters->appIsRunning)
    {
        while(!getMCUInteruptFlag())
        { 
            timeoutCount++;
            k_msleep(1);
            if(timeoutCount >= timeoutMax)
            {
                LOG_ERR("Timeout, no mcu interrupt");
                stopSequence();
                sendStatusMessage(EBoardStatusTimeout);
                return;
            }
        };
        timeoutCount = 0;
        clearMCUInteruptFlag();

        if(AD5940_WakeUp(10) > 10)
        {
            LOG_ERR("Board couldn't be woken up");
            stopSequence();
            sendStatusMessage(EBoardStatusWakeUp);
            return;
        }
        AD5940_SleepKeyCtrlS(SLPKEY_LOCK);

        uint32_t IntFlag;
        uint32_t fifoCount = 0;
        IntFlag = AD5940_INTCGetFlag(AFEINTC_0);
        if(IntFlag & AFEINTSRC_CUSTOMINT0)          /* High priority. */
        {
            LOG_INF("Reconfigure CVApp");
            AD5940Err error;
            AD5940_INTCClrFlag(AFEINTSRC_CUSTOMINT0);
            error = generateCVAppDACSequence();
            if(error != AD5940ERR_OK)
            {
                LOG_ERR("Reconfigure CVApp ERROR");
                stopSequence();
                sendStatusMessage(EBoardStatusBadRequest);
                return;
            } 
            AD5940_SleepKeyCtrlS(SLPKEY_UNLOCK);
        //AD5940_EnterSleepS(); /* If there is need to do AFE re-configure, do it here when AFE is in active state */
        }
        if(IntFlag & AFEINTSRC_DATAFIFOTHRESH)
        {   
            LOG_INF("Getting measurement CVApp");
            fifoCount = AD5940_FIFOGetCnt();
            if(fifoCount > appParameters->FifoThresh)
            {
                LOG_ERR("Measurement buffer overflow");
                stopSequence();
                sendStatusMessage(EBoardStatusBadRequest);
                return;
            }
            AD5940_FIFORd(appsAD5940IOBuffer, fifoCount);
            AD5940_INTCClrFlag(AFEINTSRC_DATAFIFOTHRESH);
            AppRAMPRegModify(&appsAD5940IOBuffer, &fifoCount);
            AD5940_SleepKeyCtrlS(SLPKEY_UNLOCK);
            AppRAMPDataProcess(appsAD5940IOBuffer, fifoCount);
            sendCVAppMessage((float*)appsAD5940IOBuffer, fifoCount);
            continue;
        }
        if(IntFlag & AFEINTSRC_ENDSEQ)
        {
            LOG_INF("finalizing CVApp");
            fifoCount = AD5940_FIFOGetCnt();
            if(fifoCount > appParameters->FifoThresh)
            {
                LOG_ERR("Measurement buffer overflow");
                stopSequence();
                sendStatusMessage(EBoardStatusBadRequest);
                return;
            }
            AD5940_INTCClrFlag(AFEINTSRC_ENDSEQ);
            AD5940_FIFORd((uint32_t *)&appsAD5940IOBuffer, fifoCount);
            AppRAMPDataProcess((int32_t *)&appsAD5940IOBuffer, fifoCount);
            sendCVAppMessage((float*) &appsAD5940IOBuffer, fifoCount);
            break;

        }
    }
    stopSequence();
    sendStatusMessage(EBoardStatusOK);
    LOG_INF("Closing CVApp");
}

