#include "Commands.h"

#include <logging/log.h>
#include "ad5940lib/ad5940.h"
#include "impedanceutilitieslib/include/Configuration.h"
#include "AppConfiguration.h"
#include "MessageSender.h"
#include "EISApp.h"
#include "CVApp.h"
#include "BoardConfiguration.h"
LOG_MODULE_REGISTER(Commands, LOG_LEVEL_DBG);

void executePingCommand(void* message)
{
	LOG_DBG("Command MessagePing");   
    sendMessage(EBoardToPcMessagePing, message, sizeof(struct MessagePing));
}

void executeConfigureSweepCommand(void* message)
{
	LOG_DBG("Command ConfigureSweep"); 
    struct MessagePcToBoardConfigureSweep* dataPtr = message;
    struct AppParameters* appParameters = getAppParametersPtr();
	appParameters->sweepConfig.SweepEn      = dataPtr->sweepEnable;
	appParameters->sweepConfig.SweepStart   = dataPtr->sweepStartFreqency;	
	appParameters->sweepConfig.SweepStop    = dataPtr->sweepStopFreqency;		
	appParameters->sweepConfig.SweepPoints  = dataPtr->sweepPoints;
	appParameters->sweepConfig.SweepLog     = dataPtr->sweepLogarythmic;
	appParameters->sweepConfig.SweepIndex   = 0;
	// LOG_DBG("\r\n\
	// SweepEn      = %d \r\n\
	// SweepStart   = %d \r\n\
	// SweepStop    = %d \r\n\
	// SweepPoints  = %d \r\n\
	// SweepLog     = %d \r\n\
	// SweepIndex   = %d \r\n\
	// ",
	// dataPtr->sweepEnable,
	// ((int)dataPtr->sweepStartFreqency),	
	// ((int)dataPtr->sweepStopFreqency),		
	// dataPtr->sweepPoints,
	// dataPtr->sweepLogarythmic,
	// 0
	// ); 
	sendStatusMessage(EBoardStatusOK);
}

void executeConfigureRampCommand(void* message)
{
	LOG_DBG("Command ConfigureSweep"); 
    struct MessagePcToBoardConfigureRamp* dataPtr = message;
    struct AppParameters* appParameters = getAppParametersPtr();
	if(dataPtr->rampStepNumber > RAMP_MAX_STEPS || dataPtr->rampStepNumber <= 0)
	{
		sendStatusMessage(EBoardStatusBadRequest);
		return;
	}
	appParameters->rampConfig.rampBothDirections = dataPtr->rampBothDirections;
	appParameters->rampConfig.rampStepNumber = dataPtr->rampStepNumber;
	appParameters->rampConfig.rampStartMiliVolt = dataPtr->rampStartMiliVolt;
	appParameters->rampConfig.rampPeakMiliVolt = dataPtr->rampPeakMiliVolt;
	appParameters->rampConfig.rampVzeroStart = dataPtr->rampVzeroStart;
	appParameters->rampConfig.rampVzeroPeak = dataPtr->rampVzeroPeak;
	appParameters->rampConfig.rampDuration = dataPtr->rampDuration;
	appParameters->rampConfig.rampSampleDelay = dataPtr->rampSampleDelay;
	sendStatusMessage(EBoardStatusOK);
}

K_THREAD_STACK_DEFINE(apps_thread_stack_area, 1024);
struct k_thread apps_thread_data;

void executeRunEISAppCommand(void* message)
{
	LOG_DBG("Starting EIS mesurement");
    struct MessagePcToBoardRunEISApp* dataPtr = message;
    struct AppParameters* appParameters = getAppParametersPtr();
	if(appParameters->appIsRunning)
	{
		appParameters->appIsRunning = false;
		k_msleep(100);
	}
	appParameters->appIsRunning = dataPtr->doRun;
	if(appParameters->appIsRunning){
		if(apps_thread_data.base.thread_state != 0){
			k_thread_abort(&apps_thread_data);
			k_thread_join(&apps_thread_data, K_NO_WAIT);
		}
		LOG_DBG("Opening app thread");
		k_thread_create(
			&apps_thread_data,
			apps_thread_stack_area,
			K_THREAD_STACK_SIZEOF(apps_thread_stack_area),
			RunEISApp,
			NULL,
			NULL,
			NULL,
			CONFIG_MAIN_THREAD_PRIORITY,
			0,
			K_NO_WAIT
		);
	}
	else{
		k_thread_abort(&apps_thread_data);
		k_thread_join(&apps_thread_data, K_NO_WAIT);
	}
}

void executeRunCVAppCommand(void* message)
{
	LOG_DBG("Starting CV mesurement");
    struct MessagePcToBoardRunCVApp* dataPtr = message;
    struct AppParameters* appParameters = getAppParametersPtr();
	appParameters->appIsRunning = 1;
	appParameters->rampConfig.rampBothDirections = 1;
    appParameters->rampConfig.rampStartMiliVolt =  -1000.0f;           /* -1V */
    appParameters->rampConfig.rampPeakMiliVolt = +1000.0f;           /* +1V */
    appParameters->rampConfig.rampVzeroStart = 1300.0f;               /* 1.3V */
    appParameters->rampConfig.rampVzeroPeak = 1300.0f;                /* 1.3V */
    appParameters->rampConfig.rampStepNumber = 512;                   /* Total steps. Equals to ADC sample number */
    appParameters->rampConfig.rampDuration = 24*1000;            /* X * 1000, where x is total duration of ramp signal. Unit is ms. */
    appParameters->rampConfig.rampSampleDelay = 7.0f;                 /* 7ms. Time between update DAC and ADC sample. Unit is ms. */

	
	if(appParameters->appIsRunning)
	{
		appParameters->appIsRunning = false;
		k_msleep(100);
	}
	appParameters->appIsRunning = dataPtr->doRun;
	if(appParameters->appIsRunning){
		if(apps_thread_data.base.thread_state != 0){
			k_thread_abort(&apps_thread_data);
			k_thread_join(&apps_thread_data, K_NO_WAIT);
		}
		LOG_DBG("Opening app thread");
		k_thread_create(
			&apps_thread_data,
			apps_thread_stack_area,
			K_THREAD_STACK_SIZEOF(apps_thread_stack_area),
			RunCVApp,
			NULL,
			NULL,
			NULL,
			CONFIG_MAIN_THREAD_PRIORITY,
			0,
			K_NO_WAIT
		);
	}
	else{
		k_thread_abort(&apps_thread_data);
		k_thread_join(&apps_thread_data, K_NO_WAIT);
	}
}