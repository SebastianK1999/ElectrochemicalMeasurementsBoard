#include "BoardConfiguration.h"

#include "impedanceutilitieslib/include/Configuration.h"
#include"MessageSender.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(BoardConfiguration, LOG_LEVEL_DBG);

const uint8_t kPreamble[PREAMBLE_SIZE] = {PREAMBLE_ARRAY_CONTENT};

int32_t convertInt32ToInt18(int32_t value)
{
    if(value & (1L<<17)) // if negative
    {
        value |= 0xfffc0000; // make it negative
    }
    else
    {
        value &= 0x0003ffff; // else, use 18bit mask
    }
    return value;
}

void convertInt32ArrayToInt18Array(int32_t* const data, uint32_t size)
{
    for(uint32_t i=0; i<size; i++)
    {
        data[i] = convertInt32ToInt18(data[i]);
    }
}

void sendStatusMessage(enum EBoardStatus status)
{
    LOG_INF("Sending status %d", status);
    static struct MessageBoardToPcStatus message;
    message.status = status;
    sendMessage(EBoardToPcMessageStatus, &message, sizeof(struct MessageBoardToPcStatus));
}
