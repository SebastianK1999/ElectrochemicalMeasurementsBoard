#include "AppConfiguration.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(AppConfiguration, LOG_LEVEL_DBG);
uint32_t appsAD5940IOBuffer[APPS_AD5940_IO_BUFFER_SIZE];

void resetBoard()
{
    LOG_INF("Reset AD5940 board");
    AD5940_HWReset();
    AD5940_Initialize();
}

AD5940Err stopSequence()
{
    LOG_INF("Stoping sequence");
    if(AD5940_WakeUp(10) > 10) 
    {
        return AD5940ERR_WAKEUP;
    }
    AD5940_WUPTCtrl(bFALSE); // todo some loop to see, if 
    /* There is chance this operation will fail because sequencer could put AFE back 
        to hibernate mode just after waking up. Use STOPSYNC is better. */
    AD5940_WUPTCtrl(bFALSE);
    AD5940_ShutDownS();
    return AD5940ERR_OK;
}

static const struct gpio_dt_spec mcuIntDevice = GPIO_DT_SPEC_GET_BY_IDX(DT_ALIAS(mcuinterupt), gpios, 0);
static struct gpio_callback mcuIntDeviceCbData;
static bool MCUInteruptFlag = false;

void setMCUInteruptFlag(const struct device *dev, struct gpio_callback *cb, uint32_t pins)
{
    LOG_INF("set MCU flag");
	MCUInteruptFlag = true;
}
void clearMCUInteruptFlag()
{
    LOG_INF("clear MCU flag");
    MCUInteruptFlag = false;
}
bool getMCUInteruptFlag()
{
    return MCUInteruptFlag;
}

void configMCUInteruptFlag()
{
    LOG_INF("Configuring MCU interupt flag");
    if (!device_is_ready(mcuIntDevice.port)) {
        LOG_ERR("Error: mcuIntDevice device %s is not ready\n", mcuIntDevice.port->name);
        return;
    }

    int ret = gpio_pin_configure_dt(&mcuIntDevice, GPIO_INPUT);
    if (ret != 0) {
        LOG_ERR("Error %d: failed to configure %s pin %d\n", ret, mcuIntDevice.port->name, mcuIntDevice.pin);
        return;
    }

    ret = gpio_pin_interrupt_configure_dt(&mcuIntDevice, GPIO_INT_EDGE_TO_ACTIVE);
    if (ret != 0) {
        LOG_ERR("Error %d: failed to configure interrupt on %s pin %d\n", ret, mcuIntDevice.port->name, mcuIntDevice.pin);
        return;
    }

    gpio_init_callback(&mcuIntDeviceCbData, setMCUInteruptFlag, BIT(mcuIntDevice.pin));
    gpio_add_callback(mcuIntDevice.port, &mcuIntDeviceCbData);
}

struct AppParameters* getAppParametersPtr()
{
    static struct AppParameters appParameters;
    return &appParameters;
}

