#include "UartIO.h"

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <drivers/uart.h>

#include <logging/log.h>

#include "MessageAnalyzer.h"
#include "impedanceutilitieslib/include/Configuration.h"

LOG_MODULE_REGISTER(uart, LOG_LEVEL_DBG);

#define UART DT_NODELABEL(usart2)
#define UART_RX_BUFFER_LEN	32
#define UART_RX_BUFFER_NUM	4
#define UART_RX_MSGQ_SIZE	32      /* Should be power of 2 */
#define UART_TIMEOUT		1    /* Milliseconds */

#define UART_TX_MSGQ_SIZE	32  /* Should be power of 2 */


K_THREAD_STACK_DEFINE(uart_tx_thread_stack_area, 512);
struct k_thread uart_tx_thread_data;
K_THREAD_STACK_DEFINE(uart_rx_thread_stack_area, 512);
struct k_thread uart_rx_thread_data;

static uint8_t rx_buffer[UART_RX_BUFFER_NUM][UART_RX_BUFFER_LEN];
static uint8_t rx_buffer_tail;
static uint8_t rx_buffer_head;
static uint8_t __aligned(4) rx_msgq_buffer[UART_RX_MSGQ_SIZE * sizeof(size_t)];
static struct k_msgq rx_msgq;

struct tx_message {
	void *data;
	size_t length;
};
static uint8_t __aligned(4) tx_msgq_buffer[UART_TX_MSGQ_SIZE * sizeof(struct tx_message)];
struct k_msgq tx_msgq;
static struct k_sem tx_sem;

/* Prepare struct for uart configuration */
static const struct uart_config uart_config = {
	.baudrate = 115200,
	.parity = UART_CFG_PARITY_NONE,
	.data_bits = UART_CFG_DATA_BITS_8,
	.stop_bits = UART_CFG_STOP_BITS_1,
	.flow_ctrl = UART_CFG_FLOW_CTRL_NONE,
};


void uart_rx_thread_entry(void *uart_dev, void *unused2, void *unused3)
{
	size_t length;
	size_t index = 0;
	uint8_t *buffer = rx_buffer[rx_buffer_tail++];

	while (true)
	{
		k_msgq_get(&rx_msgq, &length, K_FOREVER);
		
		LOG_HEXDUMP_DBG(buffer + index, length, "rx");
		
		analyseMessage(buffer + index, length);
		index += length;
		if (index == UART_RX_BUFFER_LEN)
		{
			index = 0;
			buffer = rx_buffer[rx_buffer_tail];
			rx_buffer_tail = (rx_buffer_tail + 1) % UART_RX_BUFFER_NUM;
		}
	}
}

void uart_tx_thread_entry(void *uart_dev, void *unused2, void *unused3)
{
	const struct device *uart = uart_dev;
	struct tx_message message;
	int ret;

	LOG_INF("Entering uart thread ...");

	while (1) {
		k_msgq_get(&tx_msgq, &message, K_FOREVER);
		k_sem_take(&tx_sem, K_FOREVER);
		ret = uart_tx(uart, message.data, message.length, SYS_FOREVER_MS);
		LOG_INF("sending via tx...");
		if (ret) {
			LOG_ERR("uart tx failed: %d", ret);
		}
	}
}

int uart_send_async(void* data, size_t length)
{
	struct tx_message message = {
		.data = data,
		.length = length,
	};
	if (k_msgq_put(&tx_msgq, &message, K_NO_WAIT) != 0) {
		LOG_ERR("FATAL. Uart tx overflow. Message queue full");
		return -ENOMEM;
	}
	return 0;
}

static void uart_handler(const struct device *dev, struct uart_event *event, void *user_data)
{
	switch (event->type) {
	case UART_RX_RDY:
		/* When new data arrives put length in message queue */
		if (k_msgq_put(&rx_msgq, &event->data.rx.len, K_NO_WAIT) != 0) {
			LOG_ERR("FATAL. Uart rx overflow. Message queue full");
		}

		break;
	case UART_RX_BUF_REQUEST:
		LOG_DBG("Driver requested for a new buffer. Current buffer: %d", rx_buffer_head);
		/* Check if we caught tail, if so overflow happened, and there is nothing we can do */
		if (rx_buffer_head == rx_buffer_tail) {
			LOG_ERR("FATAL. Uart rx overflow. No more free rx buffers.");
			break;
		}

		/* Give new buffer to the driver, take head, and increment head index */
		uart_rx_buf_rsp(dev, rx_buffer[rx_buffer_head], UART_RX_BUFFER_LEN);
		rx_buffer_head = (rx_buffer_head + 1) % UART_RX_BUFFER_NUM;
		break;
	case UART_RX_STOPPED:
		LOG_ERR("UART rx stopped, reason: %d", event->data.rx_stop.reason);
		break;
	case UART_RX_DISABLED:
		LOG_ERR("UART rx disabled");
		break;
	case UART_TX_DONE:
	case UART_TX_ABORTED:
		/* When transmision is done or aborted, we want to release uart */
		k_sem_give(&tx_sem);
		break;

	default:
		break;
	}
}

void init_uart(void)
{
	LOG_INF("init_uart ...");
	const struct device *uart;
	int ret;

	uart = device_get_binding(DT_LABEL(UART));
	if (uart == NULL) {
		LOG_ERR("Unable to get UART binding");
		return;
	}	

	LOG_INF("init_uart post binding ...");
	LOG_INF("uart name: %s", uart->name);

	/* Use prepared struct to configure uart */
	ret = uart_configure(uart, &uart_config);
	if (ret) {
		LOG_ERR("Unable to configure UART");
		return;
	}

	/* Configure uart callback */
	ret = uart_callback_set(uart, uart_handler, NULL);
	/* Initialise tx message queue */
	k_sem_init(&tx_sem, 1, 1); /* Program will start with semaphore set, so tx available */
	k_msgq_init(&tx_msgq, tx_msgq_buffer, sizeof(struct tx_message), UART_TX_MSGQ_SIZE);

	if (ret) {
		LOG_ERR("Unable to configure UART handler");
		return;
	}

	LOG_INF("init thread tx ...");
	/* Start uart tx thread */
	k_thread_create(&uart_tx_thread_data, uart_tx_thread_stack_area,
			K_THREAD_STACK_SIZEOF(uart_tx_thread_stack_area),
			uart_tx_thread_entry,
			(struct device *)uart, NULL, NULL,
			CONFIG_MAIN_THREAD_PRIORITY, 0, K_NO_WAIT);

	LOG_INF("init thread rx ...");
	/* Start uart rx thread */
	k_thread_create(&uart_rx_thread_data, uart_rx_thread_stack_area,
			K_THREAD_STACK_SIZEOF(uart_rx_thread_stack_area),
			uart_rx_thread_entry,
			(struct device *)uart, NULL, NULL,
			CONFIG_MAIN_THREAD_PRIORITY, 0, K_NO_WAIT);

	/* Initialise rx message queue */
	k_msgq_init(&rx_msgq, rx_msgq_buffer, sizeof(size_t), UART_RX_MSGQ_SIZE);

	/* Enable uart rx, and give first buffer to the driver */
	uart_rx_enable(uart, rx_buffer[rx_buffer_head++], UART_RX_BUFFER_LEN, UART_TIMEOUT);
}
