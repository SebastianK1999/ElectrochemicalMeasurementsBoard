#include "MessageAnalyzer.h"

#include <stdbool.h>
#include <stdio.h>

#include <logging/log.h>
#include "impedanceutilitieslib/include/Configuration.h"
#include "impedanceutilitieslib/include/Utilities.h"
#include "Commands.h"
#include "EISApp.h"
#include "BoardConfiguration.h"

LOG_MODULE_REGISTER(AnalyseMessage, LOG_LEVEL_DBG);

static bool findPreamble(MessageByteType* buffer)
{
    for (int i = 0; i < sizeof(kPreamble); ++i)
    {
        if (*(buffer + i) != kPreamble[i])
        {
            return false;
        }
    }
    return true;
}

static bool isCorrectPreamble(MessageByteType** const begin, MessageByteType* const end)
{
    while (*begin < end - sizeof(kPreamble))
    {
        if (findPreamble(*begin))
        {
            return true;
        }
        else
        {
            ++*begin;
        }
    }
    return false;
}

static bool validateControlSum(MessageByteType* const messageBegin, const size_t messageSize)
{
    return calculateControlSum(messageBegin + MESSAGE_TYPE_INDEX, messageSize + sizeof(SerializedMessageTypeType)) == (ControlSumType)*(messageBegin + CONTROL_SUM_INDEX);
}                                

static bool validateAndExecuteCommand(MessageByteType** const messageBegin, const MessageByteType* const messageEnd, const size_t messageSize, void(*command)(void*))
{
    LOG_DBG("validate");
    if (HEAD_SIZE + messageSize <= messageEnd - *messageBegin)   
    {                                                                           
        LOG_DBG("validate2");                                                   
        if (validateControlSum(*messageBegin, messageSize))      
        {                                                                       
            LOG_DBG("command");                                                 
            command(*messageBegin + MESSAGE_DATA_INDEX);                        
            *messageBegin += HEAD_SIZE + messageSize;
            return true;                                                   
        }                                                                       
        else                                                                    
        {                                                                       
            LOG_DBG("Message corrupted, dropping.");                            
            *messageBegin += sizeof(kPreamble);    
            return true;                                               
        }                                                                       
    } 
    return false;              
}
#include <kernel.h>
void analyseMessage(void* buffer, const size_t length)
{
    LOG_DBG("Entry");
    static MessageByteType messageTable[2 * (HEAD_SIZE + MAX_MESSAGE_SIZE)];
    static MessageByteType* messageBegin = messageTable;
    static MessageByteType* messageEnd = messageTable;

    if (messageEnd + length < messageTable + sizeof(messageTable))
    {
        LOG_DBG("Copying");
        memcpy(messageEnd, buffer, length);
        messageEnd += length;
    }
    else
    {
        LOG_DBG("Moving");
        int currentSize = (messageEnd - messageBegin);
        memmove(messageTable, messageBegin, currentSize); //TODO write own function to move data because memmove is slowly and using memcopy would be UB here
        messageEnd = messageTable + currentSize;
        messageBegin = messageTable;

        memcpy(messageEnd, buffer, length);
        messageEnd += length;
    }

    while(isCorrectPreamble(&messageBegin, messageEnd) && messageBegin + HEAD_SIZE <= messageEnd)
    {
        LOG_DBG("analize while");
        k_msleep(10);
        switch((enum EPcToBoardMessage)(SerializedMessageTypeType)(*(messageBegin + MESSAGE_TYPE_INDEX)))
        {
        case EPcToBoardMessagePing:
            if(!validateAndExecuteCommand(&messageBegin, messageEnd, sizeof(struct MessagePing), &executePingCommand))
            {
                return;
            }
            break;
        case EPcToBoardMessageConfigureSweep:
            if(!validateAndExecuteCommand(&messageBegin, messageEnd, sizeof(struct MessagePcToBoardConfigureSweep), &executeConfigureSweepCommand))
            {
                return;
            }
            break;
        case EPcToBoardMessageConfigureRamp:
            if(!validateAndExecuteCommand(&messageBegin, messageEnd, sizeof(struct MessagePcToBoardConfigureRamp), &executeConfigureRampCommand))
            {
                return;
            }
            break;
        case EPcToBoardMessageRunEISApp:
            if(!validateAndExecuteCommand(&messageBegin, messageEnd, sizeof(struct MessagePcToBoardRunEISApp), &executeRunEISAppCommand))
            {
                return;
            }
            break;
        case EPcToBoardMessageRunCVApp:
            if(!validateAndExecuteCommand(&messageBegin, messageEnd, sizeof(struct MessagePcToBoardRunCVApp), &executeRunCVAppCommand))
            {
                return;
            }
            break;
        default:
            LOG_DBG("Unrecognized message. The message will be dropped.");
            ++messageBegin;
            break;
        }
    }
}