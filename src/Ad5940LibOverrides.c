#include "Ad5940LibOverrides.h"

#include <zephyr.h>
#include <drivers/gpio.h>
#include <drivers/spi.h>
#include <devicetree/gpio.h>

#include <logging/log.h>

#include "mathDeclarations.h"

LOG_MODULE_REGISTER(AD5940_Overrides, LOG_LEVEL_DBG);

void AD5940_CsClr(void)
{
    const static struct device* cs_bus = DEVICE_DT_GET(DT_NODELABEL(gpiob));
    gpio_pin_configure(cs_bus, 6, GPIO_OUTPUT | GPIO_PULL_DOWN);
    gpio_pin_set(cs_bus, 6, 0);
}

void AD5940_CsSet(void)
{
    const static struct device* cs_bus = DEVICE_DT_GET(DT_NODELABEL(gpiob));
    gpio_pin_configure(cs_bus, 6, GPIO_OUTPUT | GPIO_PULL_DOWN);
    gpio_pin_set(cs_bus, 6, 1);
}

void AD5940_RstSet(void)
{
    const static struct device* reset_bus = DEVICE_DT_GET(DT_NODELABEL(gpiob));
    gpio_pin_configure(reset_bus, 0, GPIO_OUTPUT | GPIO_PULL_DOWN);
    gpio_pin_set(reset_bus, 0, 1);
}

void AD5940_RstClr(void)
{
    const static struct device* reset_bus = DEVICE_DT_GET(DT_NODELABEL(gpiob));
    gpio_pin_configure(reset_bus, 0, GPIO_OUTPUT | GPIO_PULL_DOWN);
    gpio_pin_set(reset_bus, 0, 0);
}

void AD5940_Delay10us(uint32_t time)
{
    k_usleep(10);
}


void AD5940_ReadWriteNBytes(unsigned char* pSendBuffer,unsigned char* pRecvBuff,unsigned long length)
{
    static const struct device* spi_bus = DEVICE_DT_GET(DT_NODELABEL(spi1));
    static const struct spi_cs_control spi_cs = {
        .gpio_dev = DEVICE_DT_GET(DT_NODELABEL(gpioa)),
        .gpio_pin = 4,
        .gpio_dt_flags = GPIO_ACTIVE_LOW,
        .delay = 0,
    };
    static struct spi_config spi_cfg = 
    {
        .operation = SPI_WORD_SET(8) | SPI_TRANSFER_MSB | SPI_OP_MODE_MASTER,
        .frequency = 1000000U,
        .slave = 0,
        .cs = &spi_cs
    };
    const struct spi_buf tx_buf[] = 
    {
        {
            .buf = pSendBuffer,
            .len = length 
        },
    };
    const struct spi_buf rx_buf[] = 
    {
        {
            .buf = pRecvBuff,
            .len = length 
        },
    };
    struct spi_buf_set tx = 
    {
        .buffers = tx_buf,
        .count = 1,
    };
    struct spi_buf_set rx = 
    {
        .buffers = rx_buf,
        .count = 1,
    };
    spi_transceive(spi_bus, &spi_cfg, &tx, &rx);
}

void overriden_AD5940_SweepNext(SoftSweepCfg_Type *pSweepCfg, float *pNextFreq)
{
   float frequency;

   if(pSweepCfg->SweepLog)/* Log step */
   {
      if(pSweepCfg->SweepStart<pSweepCfg->SweepStop) /* Normal */
      {
         if(++pSweepCfg->SweepIndex == pSweepCfg->SweepPoints)
            pSweepCfg->SweepIndex = 0;
         frequency = pSweepCfg->SweepStart*pow(10,pSweepCfg->SweepIndex*log10(pSweepCfg->SweepStop/pSweepCfg->SweepStart)/(pSweepCfg->SweepPoints-1));
      }
      else
      {
         pSweepCfg->SweepIndex --;
         if(pSweepCfg->SweepIndex >= pSweepCfg->SweepPoints)
            pSweepCfg->SweepIndex = pSweepCfg->SweepPoints-1;
         frequency = pSweepCfg->SweepStop*pow(10,pSweepCfg->SweepIndex*
                                     (log10(pSweepCfg->SweepStart/pSweepCfg->SweepStop)/(pSweepCfg->SweepPoints-1)));
      }
   }
   else/* Linear step */
   {
      if(pSweepCfg->SweepStart<pSweepCfg->SweepStop) /* Normal */
      {
         if(++pSweepCfg->SweepIndex == pSweepCfg->SweepPoints)
            pSweepCfg->SweepIndex = 0;
         frequency = pSweepCfg->SweepStart + pSweepCfg->SweepIndex*(double)(pSweepCfg->SweepStop-pSweepCfg->SweepStart)/(pSweepCfg->SweepPoints-1);
      }
      else
      {
         pSweepCfg->SweepIndex --;
         if(pSweepCfg->SweepIndex >= pSweepCfg->SweepPoints)
            pSweepCfg->SweepIndex = pSweepCfg->SweepPoints-1;
         frequency = pSweepCfg->SweepStop + pSweepCfg->SweepIndex*(double)(pSweepCfg->SweepStart - pSweepCfg->SweepStop)/(pSweepCfg->SweepPoints-1);
      }
   }
   
   *pNextFreq = frequency;
}