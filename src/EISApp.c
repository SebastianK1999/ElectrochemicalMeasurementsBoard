#include "EISApp.h"

#include <math.h>
#include <stdbool.h>
#include <logging/log.h>
LOG_MODULE_REGISTER(RunEISApp, LOG_LEVEL_DBG);
#include "Ad5940LibOverrides.h"
#include "ad5940lib/ad5940.h"
#include "impedanceutilitieslib/include/Configuration.h"
#include "AppConfiguration.h"
#include "UartIO.h"
#include "BoardConfiguration.h"
#include "MessageSender.h"
#include "mathDeclarations.h"


#define EIS_APP_ONE_MEASUREMENT_SIZE 4
// Default LPDAC resolution(2.5V internal reference). 
#define DAC12BITVOLT_1LSB   (2200.0f/4095)  //mV
#define DAC6BITVOLT_1LSB    (DAC12BITVOLT_1LSB*64)  //mV

static void configEISAppSystemParametersSequencer()
{
    LOG_INF("Configuring sequence parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    appParameters->seqencerConfig.SeqEnable = bFALSE;
    appParameters->seqencerConfig.SeqBreakEn = bFALSE;
    appParameters->seqencerConfig.SeqIgnoreEn = bTRUE;
    appParameters->seqencerConfig.SeqCntCRCClr = bTRUE;
    appParameters->seqencerConfig.SeqMemSize = SEQUENCE_MEMORY_SIZE; // SEQMEMSIZE_2KB;
    appParameters->seqencerConfig.SeqWrTimer = 0; 
    // depends on SeqMemSize, one instruction takes 4 bytes
    AD5940_SEQCfg(&(appParameters->seqencerConfig));
}

static void configEISAppSystemParametersClock()
{
    LOG_INF("Configuring clock parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    appParameters->clockConfig.HfOSC32MHzMode = bFALSE;
    appParameters->clockConfig.HFOSCEn = bTRUE;
    appParameters->clockConfig.HFXTALEn = bFALSE;
    appParameters->clockConfig.LFOSCEn = bTRUE;
    appParameters->clockConfig.ADCClkDiv = ADCCLKDIV_1;
    appParameters->clockConfig.ADCCLkSrc = ADCCLKSRC_HFOSC;
    appParameters->clockConfig.SysClkDiv = SYSCLKDIV_1;
    appParameters->clockConfig.SysClkSrc = SYSCLKSRC_HFOSC;
    AD5940_CLKCfg(&(appParameters->clockConfig));
}

static void configEISAppSystemParametersFifo()
{
    LOG_INF("Configuring fifo parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    AD5940_FIFOCtrlS(FIFOSRC_DFT, bFALSE);
    appParameters->fifoConfig.FIFOMode = FIFOMODE_FIFO;
    appParameters->fifoConfig.FIFOSize = FIFOSIZE_4KB;
    appParameters->fifoConfig.FIFOSrc = FIFOSRC_DFT;
    appParameters->fifoConfig.FIFOThresh = EIS_APP_ONE_MEASUREMENT_SIZE;
    appParameters->fifoConfig.FIFOEn = bTRUE;
    AD5940_FIFOCfg(&(appParameters->fifoConfig));
    AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
}

static void configEISAppSystemParametersGpio()
{
    LOG_INF("Configuring gpio parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    appParameters->gpioConfig.FuncSet = GP0_INT|GP1_SLEEP|GP2_SYNC;
    appParameters->gpioConfig.InputEnSet = 0;
    appParameters->gpioConfig.OutputEnSet = AGPIO_Pin0|AGPIO_Pin1|AGPIO_Pin2;
    appParameters->gpioConfig.OutVal = 0;
    appParameters->gpioConfig.PullEnSet = 0;
    AD5940_AGPIOCfg(&(appParameters->gpioConfig));
}

static void configEISAppIteruptConfiguration()
{
    LOG_INF("Configuring interupts");
    AD5940_INTCCfg(AFEINTC_1, AFEINTSRC_ALLINT, bTRUE);
    AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
    AD5940_INTCCfg(AFEINTC_0, AFEINTSRC_DATAFIFOTHRESH, bTRUE); 
    AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
}

static void configEISAppOtherParameters()
{
    LOG_INF("Configuring other parameters");
    struct AppParameters* appParameters = getAppParametersPtr();
    appParameters->sweepCurrentFrequency = 0;
    appParameters->sweepNextFrequency = 0;
    appParameters->dataFrequency = 0;
    appParameters->biasVolt = 0.0f,
    appParameters->dacVoltagePeakToPeak = 800.0,
    appParameters->calibrationResistorValue = 10000.0;
    appParameters->impedanceOutputDataRate = 20.0,
    appParameters->systemClockFrequency = 16000000.0,
    appParameters->wakeUpTimerClockFrequency = 32000.0,
    appParameters->adcClockFrequency = 16000000.0,
    appParameters->sequencerStartAddress = 0;    
    appParameters->dftNumber = DFTNUM_16384;
    appParameters->dftSource = DFTSRC_SINC3;
    appParameters->ADCSinc2Osr = ADCSINC2OSR_22,
	appParameters->ADCSinc3Osr = ADCSINC3OSR_2;		// Sample rate is 800kSPS/2 = 400kSPS
    appParameters->adcAverageNumber = ADCAVGNUM_16,
    appParameters->hanningWindowEnabled = true;
    memset(&(appParameters->initSequenceInfo), 0, sizeof(appParameters->initSequenceInfo));
    memset(&(appParameters->measureSequenceInfo), 0, sizeof(appParameters->measureSequenceInfo));
}

AD5940Err generateEISAppInitialSequence()
{
    LOG_INF("Generating initial sequence");
    struct AppParameters* appParameters = getAppParametersPtr();
    AD5940Err error = AD5940ERR_OK;
    const uint32_t *pSeqenceCommand;
    uint32_t sequenceLength;
    AFERefCfg_Type afeReferenceConfig;
	LPLoopCfg_Type lowPowerLoopConfig;
    HSLoopCfg_Type highSpeedLoopConfig;
    DSPCfg_Type digitalSignalProcessorConfig;

    // Start sequence generator here
    AD5940_SEQGenCtrl(bTRUE);
    AD5940_AFECtrlS(AFECTRL_ALL, bFALSE);

    // Configure AFE reference
    afeReferenceConfig.HpBandgapEn = bTRUE;
    afeReferenceConfig.Hp1V1BuffEn = bTRUE;
    afeReferenceConfig.Hp1V8BuffEn = bTRUE;
    afeReferenceConfig.Disc1V1Cap = bFALSE;
    afeReferenceConfig.Disc1V8Cap = bFALSE;
    afeReferenceConfig.Hp1V8ThemBuff = bFALSE;
    afeReferenceConfig.Hp1V8Ilimit = bFALSE;
    afeReferenceConfig.Lp1V1BuffEn = bFALSE;
    afeReferenceConfig.Lp1V8BuffEn = bFALSE;
    afeReferenceConfig.LpBandgapEn = bTRUE;
    afeReferenceConfig.LpRefBufEn = bTRUE;
    afeReferenceConfig.LpRefBoostEn = bFALSE;
    AD5940_REFCfgS(&afeReferenceConfig);	
	
    // Configure low power loop
    // // Low power DAC
	lowPowerLoopConfig.LpDacCfg.LpDacSrc = LPDACSRC_MMR;
    lowPowerLoopConfig.LpDacCfg.LpDacSW = LPDACSW_VBIAS2LPPA|LPDACSW_VBIAS2PIN|LPDACSW_VZERO2LPTIA|LPDACSW_VZERO2PIN;
    lowPowerLoopConfig.LpDacCfg.LpDacVzeroMux = LPDACVZERO_6BIT;
    lowPowerLoopConfig.LpDacCfg.LpDacVbiasMux = LPDACVBIAS_12BIT;
    lowPowerLoopConfig.LpDacCfg.LpDacRef = LPDACREF_2P5;
    lowPowerLoopConfig.LpDacCfg.DataRst = bFALSE;
    lowPowerLoopConfig.LpDacCfg.PowerEn = bTRUE;
    lowPowerLoopConfig.LpDacCfg.DacData6Bit = (uint32_t)((-200)/DAC6BITVOLT_1LSB);
    lowPowerLoopConfig.LpDacCfg.DacData12Bit =(int32_t)((appParameters->biasVolt)/DAC12BITVOLT_1LSB) + lowPowerLoopConfig.LpDacCfg.DacData6Bit*64;
    if(lowPowerLoopConfig.LpDacCfg.DacData12Bit>lowPowerLoopConfig.LpDacCfg.DacData6Bit*64)
    {
        lowPowerLoopConfig.LpDacCfg.DacData12Bit--;
    }
    // // Low power AMP
    lowPowerLoopConfig.LpAmpCfg.LpAmpPwrMod = LPAMPPWR_NORM;
    lowPowerLoopConfig.LpAmpCfg.LpPaPwrEn = bTRUE;
    lowPowerLoopConfig.LpAmpCfg.LpTiaPwrEn = bTRUE;
    lowPowerLoopConfig.LpAmpCfg.LpTiaRf = LPTIARF_1M;
    lowPowerLoopConfig.LpAmpCfg.LpTiaRload = LPTIARLOAD_100R;
    lowPowerLoopConfig.LpAmpCfg.LpTiaRtia = LPTIARTIA_4K;
    lowPowerLoopConfig.LpAmpCfg.LpTiaSW = LPTIASW(5)|LPTIASW(2)|LPTIASW(4)|LPTIASW(12)|LPTIASW(13); 
    AD5940_LPLoopCfgS(&lowPowerLoopConfig);

    // High speed loop configuration
    // // High speed DAC
    highSpeedLoopConfig.HsDacCfg.ExcitBufGain = EXCITBUFGAIN_2;
    highSpeedLoopConfig.HsDacCfg.HsDacGain = HSDACGAIN_1;
    highSpeedLoopConfig.HsDacCfg.HsDacUpdateRate = 7;
    // // High speed "Thank in advance"
    highSpeedLoopConfig.HsTiaCfg.DiodeClose = bFALSE;
    highSpeedLoopConfig.HsTiaCfg.HstiaBias = HSTIABIAS_1P1;
    highSpeedLoopConfig.HsTiaCfg.HstiaCtia = 31; // 31pF + 2pF 
    highSpeedLoopConfig.HsTiaCfg.HstiaDeRload = HSTIADERLOAD_OPEN;
    highSpeedLoopConfig.HsTiaCfg.HstiaDeRtia = HSTIADERTIA_OPEN;
    highSpeedLoopConfig.HsTiaCfg.HstiaRtiaSel = HSTIARTIA_5K;
    // // Switch matrix
    highSpeedLoopConfig.SWMatCfg.Dswitch = SWD_CE0;
    highSpeedLoopConfig.SWMatCfg.Pswitch = SWP_RE0;
    highSpeedLoopConfig.SWMatCfg.Nswitch = SWN_SE0;
    highSpeedLoopConfig.SWMatCfg.Tswitch = SWT_TRTIA | SWT_SE0LOAD;
    // // Calculate next sweep frequency
    if(appParameters->sweepConfig.SweepEn == bTRUE)
    {
        appParameters->dataFrequency = appParameters->sweepConfig.SweepStart;
        appParameters->sweepCurrentFrequency = appParameters->sweepConfig.SweepStart;
        overriden_AD5940_SweepNext(&(appParameters->sweepConfig), &(appParameters->sweepNextFrequency));
    }
    else
    {
        appParameters->dataFrequency = appParameters->sweepConfig.SweepStart;
        appParameters->sweepCurrentFrequency = appParameters->sweepConfig.SweepStart;
        appParameters->sweepNextFrequency = appParameters->sweepConfig.SweepStart;
    }
    // // Waveform generator
    highSpeedLoopConfig.WgCfg.WgType = WGTYPE_SIN;
    highSpeedLoopConfig.WgCfg.GainCalEn = bTRUE;
    highSpeedLoopConfig.WgCfg.OffsetCalEn = bTRUE;
    highSpeedLoopConfig.WgCfg.SinCfg.SinFreqWord = AD5940_WGFreqWordCal(appParameters->sweepCurrentFrequency, appParameters->systemClockFrequency);
	highSpeedLoopConfig.WgCfg.SinCfg.SinAmplitudeWord = (uint32_t)(appParameters->dacVoltagePeakToPeak/800.0f*2047 + 0.5f);
    highSpeedLoopConfig.WgCfg.SinCfg.SinOffsetWord = 0;
    highSpeedLoopConfig.WgCfg.SinCfg.SinPhaseWord = 0;
    AD5940_HSLoopCfgS(&highSpeedLoopConfig);

    // Configure digital signal processor
    // // ADC base 
    digitalSignalProcessorConfig.ADCBaseCfg.ADCMuxN = ADCMUXN_HSTIA_N;
    digitalSignalProcessorConfig.ADCBaseCfg.ADCMuxP = ADCMUXP_HSTIA_P;
    digitalSignalProcessorConfig.ADCBaseCfg.ADCPga = ADCPGA_1;
    memset(&digitalSignalProcessorConfig.ADCDigCompCfg, 0, sizeof(digitalSignalProcessorConfig.ADCDigCompCfg));
    // // ADC filter
    digitalSignalProcessorConfig.ADCFilterCfg.ADCAvgNum = appParameters->adcAverageNumber;
    digitalSignalProcessorConfig.ADCFilterCfg.ADCRate = ADCRATE_800KHZ;	// Tell filter block clock rate of ADC
    digitalSignalProcessorConfig.ADCFilterCfg.ADCSinc2Osr = appParameters->ADCSinc2Osr;
    digitalSignalProcessorConfig.ADCFilterCfg.ADCSinc3Osr = appParameters->ADCSinc3Osr;
    digitalSignalProcessorConfig.ADCFilterCfg.BpNotch = bTRUE;
    digitalSignalProcessorConfig.ADCFilterCfg.BpSinc3 = bFALSE;
    digitalSignalProcessorConfig.ADCFilterCfg.Sinc2NotchEnable = bTRUE;
    // // DFT
    digitalSignalProcessorConfig.DftCfg.DftNum = appParameters->dftNumber;
    digitalSignalProcessorConfig.DftCfg.DftSrc = appParameters->dftSource;
    digitalSignalProcessorConfig.DftCfg.HanWinEn = appParameters->hanningWindowEnabled;
    memset(&digitalSignalProcessorConfig.StatCfg, 0, sizeof(digitalSignalProcessorConfig.StatCfg));
    AD5940_DSPCfgS(&digitalSignalProcessorConfig);
        
    // Enable all of them. They are automatically turned off during hibernate mode to save power
    uint32_t afeControlParameter =  AFECTRL_HSTIAPWR    | 
                                    AFECTRL_INAMPPWR    |
                                    AFECTRL_EXTBUFPWR   |
                                    AFECTRL_WG          |
                                    AFECTRL_DACREFPWR   |
                                    AFECTRL_HSDACPWR    |
                                    AFECTRL_SINC2NOTCH;
    if(appParameters->biasVolt != 0.0f)
    {
        afeControlParameter |= AFECTRL_DCBUFPWR;
    }
    AD5940_AFECtrlS(afeControlParameter, bTRUE);
    
    // Sequence end.
    AD5940_SEQGenInsert(SEQ_STOP()); // Add one extra command to disable sequencer for initialization sequence because we only want it to run one time.

    // Stop here
    error = AD5940_SEQGenFetchSeq(&pSeqenceCommand, &sequenceLength);
    AD5940_SEQGenCtrl(bFALSE); // Stop sequencer generator
    if(error == AD5940ERR_OK)
    {
        appParameters->initSequenceInfo.SeqId = SEQID_1;
        appParameters->initSequenceInfo.SeqRamAddr = appParameters->sequencerStartAddress;
        appParameters->initSequenceInfo.pSeqCmd = pSeqenceCommand;
        appParameters->initSequenceInfo.SeqLen = sequenceLength;
        AD5940_SEQCmdWrite(appParameters->initSequenceInfo.SeqRamAddr, pSeqenceCommand, sequenceLength);
    }
    return error;
}

AD5940Err generateEISAppMeasurementSequence()
{
    LOG_INF("Generating measurement sequence");
    struct AppParameters* appParameters = getAppParametersPtr();
    AD5940Err error = AD5940ERR_OK;
    const uint32_t *pSeqenceCommand;
    uint32_t sequenceLength;
    uint32_t WaitClks;
    SWMatrixCfg_Type switchConfig;
    ClksCalInfo_Type calculateClockConfig;
    // Config Clock calculation
    calculateClockConfig.DataType = DATATYPE_DFT;
    calculateClockConfig.DftSrc = appParameters->dftSource;
    calculateClockConfig.DataCount = 1L<<(appParameters->dftNumber+2); // 2^(DFTNUMBER+2) 
    calculateClockConfig.ADCSinc2Osr = appParameters->ADCSinc2Osr;
    calculateClockConfig.ADCSinc3Osr = appParameters->ADCSinc3Osr;
    calculateClockConfig.ADCAvgNum = appParameters->adcAverageNumber;
    calculateClockConfig.RatioSys2AdcClk = appParameters->systemClockFrequency/appParameters->adcClockFrequency;
    AD5940_ClksCalculate(&calculateClockConfig, &WaitClks);

    AD5940_SEQGenCtrl(bTRUE);
    AD5940_SEQGpioCtrlS(AGPIO_Pin2); // Set GPIO1, clear others that under control
    AD5940_SEQGenInsert(SEQ_WAIT(16*250));  // @todo wait 250us?
    switchConfig.Dswitch = SWD_RCAL0;
    switchConfig.Pswitch = SWP_RCAL0;
    switchConfig.Nswitch = SWN_RCAL1;
    switchConfig.Tswitch = SWT_RCAL1|SWT_TRTIA;
    AD5940_SWMatrixCfgS(&switchConfig);
    uint32_t afeControlParameter =  AFECTRL_HSTIAPWR    | 
                                    AFECTRL_INAMPPWR    |
                                    AFECTRL_EXTBUFPWR   |
                                    AFECTRL_WG          |
                                    AFECTRL_DACREFPWR   |
                                    AFECTRL_HSDACPWR    |
                                    AFECTRL_SINC2NOTCH;
    if(appParameters->biasVolt != 0.0f)
    {
        afeControlParameter |= AFECTRL_DCBUFPWR;
    }
	AD5940_AFECtrlS(afeControlParameter, bTRUE);
    AD5940_AFECtrlS(AFECTRL_WG|AFECTRL_ADCPWR, bTRUE);  // Enable Waveform generator
    //delay for signal settling DFT_WAIT
    AD5940_SEQGenInsert(SEQ_WAIT(16*10));
    AD5940_AFECtrlS(AFECTRL_ADCCNV|AFECTRL_DFT, bTRUE);  // Start ADC convert and DFT
    AD5940_SEQGenInsert(SEQ_WAIT(WaitClks));
    //wait for first data ready
    AD5940_AFECtrlS(AFECTRL_ADCPWR|AFECTRL_ADCCNV|AFECTRL_DFT|AFECTRL_WG, bFALSE);  // Stop ADC convert and DFT

    switchConfig.Dswitch = SWD_CE0;
    switchConfig.Pswitch = SWP_RE0;
    switchConfig.Nswitch = SWN_SE0;
    switchConfig.Tswitch = SWT_TRTIA | SWT_SE0LOAD;
    AD5940_SWMatrixCfgS(&switchConfig);
    AD5940_AFECtrlS(AFECTRL_ADCPWR|AFECTRL_WG, bTRUE);  // Enable Waveform generator
    AD5940_SEQGenInsert(SEQ_WAIT(16*10));  // delay for signal settling DFT_WAIT
    AD5940_AFECtrlS(AFECTRL_ADCCNV|AFECTRL_DFT, bTRUE);  // Start ADC convert and DFT
    AD5940_SEQGenInsert(SEQ_WAIT(WaitClks));  // wait for first data ready
    AD5940_AFECtrlS(AFECTRL_ADCCNV|AFECTRL_DFT|AFECTRL_WG|AFECTRL_ADCPWR, bFALSE);  // Stop ADC convert and DFT
    AD5940_AFECtrlS(afeControlParameter, bFALSE);
    AD5940_SEQGpioCtrlS(0); // Clr GPIO1

    AD5940_EnterSleepS();// Goto hibernate

    // Sequence end.
    error = AD5940_SEQGenFetchSeq(&pSeqenceCommand, &sequenceLength);
    AD5940_SEQGenCtrl(bFALSE); // Stop sequencer generator

    if(error == AD5940ERR_OK)
    {
        appParameters->measureSequenceInfo.SeqId = SEQID_0;
        appParameters->measureSequenceInfo.SeqRamAddr = appParameters->initSequenceInfo.SeqRamAddr + appParameters->initSequenceInfo.SeqLen ;
        appParameters->measureSequenceInfo.pSeqCmd = pSeqenceCommand;
        appParameters->measureSequenceInfo.SeqLen = sequenceLength;
        // Write command to SRAM
        AD5940_SEQCmdWrite(appParameters->measureSequenceInfo.SeqRamAddr, pSeqenceCommand, sequenceLength);
    }
    return error; 
}

AD5940Err configEISAppInitializationSequence()
{
    LOG_INF("Initialize sequence");
    struct AppParameters* appParameters = getAppParametersPtr();
    
    AD5940_SEQGenInit(appsAD5940IOBuffer, APPS_AD5940_IO_BUFFER_SIZE);

    AD5940Err error;
    error = generateEISAppInitialSequence(); // Application initialization sequence using either MCU or sequencer
    if(error != AD5940ERR_OK)
    {
        return error;
    }
    error = generateEISAppMeasurementSequence();
    if(error != AD5940ERR_OK)
    {
        return error;
    }

    appParameters->initSequenceInfo.WriteSRAM = bFALSE;
    AD5940_SEQInfoCfg(&(appParameters->initSequenceInfo));
    appParameters->seqencerConfig.SeqEnable = bTRUE;
    AD5940_SEQCfg(&(appParameters->seqencerConfig));  // Enable sequencer 
    AD5940_SEQMmrTrig(appParameters->initSequenceInfo.SeqId);
    while(AD5940_INTCTestFlag(AFEINTC_1, AFEINTSRC_ENDSEQ) == bFALSE)
    {
        k_msleep(1);
    }

    appParameters->measureSequenceInfo.WriteSRAM = bFALSE;
    AD5940_SEQInfoCfg(&(appParameters->measureSequenceInfo));
    appParameters->seqencerConfig.SeqEnable = bTRUE;
    AD5940_SEQCfg(&(appParameters->seqencerConfig));  // Enable sequencer, and wait for trigger 
    clearMCUInteruptFlag();   // Clear interrupt flag generated before 
    AD5940_AFEPwrBW(AFEPWR_HP, AFEBW_250KHZ);
    return AD5940ERR_OK;
}

AD5940Err configEISAppParametersWakeUpTimer() // app imp control start
{
    LOG_INF("Configure wake up timer parameter");
    struct AppParameters* appParameters = getAppParametersPtr();
    if(AD5940_WakeUp(10) > 10)
    {
        return AD5940ERR_WAKEUP;
    }
    appParameters->wakeUpTimerConfig.WuptEn = bTRUE;
    appParameters->wakeUpTimerConfig.WuptEndSeq = WUPTENDSEQ_A;
    appParameters->wakeUpTimerConfig.WuptOrder[0] = SEQID_0;
    appParameters->wakeUpTimerConfig.SeqxSleepTime[SEQID_0] = 4;
    appParameters->wakeUpTimerConfig.SeqxWakeupTime[SEQID_0] = (uint32_t)(appParameters->wakeUpTimerClockFrequency/appParameters->impedanceOutputDataRate)-4;
    AD5940_WUPTCfg(&(appParameters->wakeUpTimerConfig));
    return AD5940ERR_OK;
}

fImpPol_Type EISAppProcessData(uint32_t* const data, uint32_t measurementsCount)
{
    LOG_INF("Processing data");
    struct AppParameters* appParameters = getAppParametersPtr();
    fImpPol_Type returnValue;
    iImpCar_Type *pDftCalibration = ((iImpCar_Type*) data) + 0;
    iImpCar_Type *pDftMeasurement = ((iImpCar_Type*) data) + 1;

    float measurementMagnitude = sqrt(((float)pDftMeasurement->Real) * ((float)pDftMeasurement->Real) + ((float)pDftMeasurement->Image) * ((float)pDftMeasurement->Image));
    float measurmentPhase = atan2(-(pDftMeasurement->Image), (pDftMeasurement->Real));
    float calibrationMagnitude = sqrt(((float)pDftCalibration->Real) * ((float)pDftCalibration->Real) + ((float)pDftCalibration->Image) * ((float)pDftCalibration->Image));
    float calibrationPhase = atan2(-(pDftCalibration->Image), (pDftCalibration->Real));
    measurementMagnitude = (calibrationMagnitude/measurementMagnitude) * (appParameters->calibrationResistorValue);
    measurmentPhase = calibrationPhase - measurmentPhase;
    if(measurmentPhase < 0) measurmentPhase = (2 * MATH_PI) + measurmentPhase;

    returnValue.Magnitude = measurementMagnitude;
    returnValue.Phase = measurmentPhase;
    return returnValue;
}

void sendEISAppMessage(fImpPol_Type measurement)
{
    LOG_INF("Sending message");
    struct AppParameters* appParameters = getAppParametersPtr();
    struct MessageBoardToPcEISMeasurement message;
    message.frequency = appParameters->dataFrequency;
    message.phase = measurement.Phase;
    message.magnitude = measurement.Magnitude;
    sendMessage(EBoardToPcMessageEISMeasurement, &message, sizeof(struct MessageBoardToPcEISMeasurement));
}

void RunEISApp()
{ 
    LOG_INF("Running EISApp");
    struct AppParameters* appParameters = getAppParametersPtr();
    AD5940Err error = 0;
    resetBoard();
    AD5940_SleepKeyCtrlS(SLPKEY_UNLOCK);   
    configEISAppSystemParametersClock();
    configEISAppIteruptConfiguration();
    configEISAppSystemParametersGpio();
    configEISAppOtherParameters();
    configEISAppSystemParametersSequencer();
    configEISAppSystemParametersFifo();
    error = configEISAppInitializationSequence();
    error = configEISAppParametersWakeUpTimer();
    if(error != AD5940ERR_OK)
    {
        LOG_ERR("EISApp Configuration Error");
        stopSequence();
        switch (error)
        {
        case AD5940ERR_WAKEUP:
            sendStatusMessage(EBoardStatusWakeUp);
            break;
        
        default:
            sendStatusMessage(EBoardStatusBadRequest);
            break;
        }
        return;
    }

    size_t measurementsCount = 0;
    uint32_t measurementBuffer[EIS_APP_ONE_MEASUREMENT_SIZE];
    uint32_t timeoutCount = 0;
    while(measurementsCount < appParameters->sweepConfig.SweepPoints && appParameters->appIsRunning)
    {
        LOG_INF("Measure %d of %d", measurementsCount + 1, appParameters->sweepConfig.SweepPoints);
        while(!getMCUInteruptFlag())
        { 
            timeoutCount++;
            k_msleep(1);
            if(timeoutCount >= EISAPP_TIMEOUT_MILISECONDS)
            {
                LOG_ERR("Timeout, no mcu interrupt");
                stopSequence();
                sendStatusMessage(EBoardStatusTimeout);
                return;
            }
        };
        clearMCUInteruptFlag();

        if(AD5940_WakeUp(10) > 10)
        {
            LOG_ERR("Board couldn't be woken up");
            stopSequence();
            sendStatusMessage(EBoardStatusWakeUp);
            return;
        }
        AD5940_SleepKeyCtrlS(SLPKEY_LOCK);  // Prohibit AFE to enter sleep mode. 

        if(AD5940_INTCTestFlag(AFEINTC_0, AFEINTSRC_DATAFIFOTHRESH) == bTRUE)
        {
            LOG_INF("Geting measurement count");
            uint32_t measurementsRead = (AD5940_FIFOGetCnt() / EIS_APP_ONE_MEASUREMENT_SIZE);
            uint32_t dataRead = measurementsRead * EIS_APP_ONE_MEASUREMENT_SIZE;
            timeoutCount = 0;
            if(dataRead != EIS_APP_ONE_MEASUREMENT_SIZE)
            {
                AD5940_WGFreqCtrlS(appParameters->sweepCurrentFrequency, appParameters->systemClockFrequency);
                AD5940_SleepKeyCtrlS(SLPKEY_UNLOCK);
                continue;
            }
            LOG_INF("Reading fifo");
            AD5940_FIFORd(measurementBuffer, dataRead);
            AD5940_INTCClrFlag(AFEINTSRC_DATAFIFOTHRESH);
            if(appParameters->sweepConfig.SweepEn)
            {
                appParameters->dataFrequency = appParameters->sweepCurrentFrequency;
                appParameters->sweepCurrentFrequency = appParameters->sweepNextFrequency;
                overriden_AD5940_SweepNext(&(appParameters->sweepConfig), &(appParameters->sweepNextFrequency));
                AD5940_WGFreqCtrlS(appParameters->sweepCurrentFrequency, appParameters->systemClockFrequency);
            }
            AD5940_SleepKeyCtrlS(SLPKEY_UNLOCK);
            convertInt32ArrayToInt18Array(measurementBuffer, dataRead);
            fImpPol_Type measurement = EISAppProcessData(measurementBuffer, measurementsRead);
            sendEISAppMessage(measurement);
            measurementsCount++;
        }
    }
    stopSequence();
    sendStatusMessage(EBoardStatusOK);
    LOG_INF("Closing EISApp");
}