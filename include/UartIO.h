#ifndef UARTIO_INCLUDED
#define UARTIO_INCLUDED

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <drivers/uart.h>

extern struct k_msgq tx_msgq;

void uart_rx_thread_entry(void *uart_dev, void *unused2, void *unused3);
void uart_tx_thread_entry(void *uart_dev, void *unused2, void *unused3);
void init_uart(void);
int uart_send_async(void *buff, size_t n);
int uart_copy_send_async(void* data, size_t length);

#endif