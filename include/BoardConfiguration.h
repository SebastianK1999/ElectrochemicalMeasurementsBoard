#ifndef BOARD_CONFIGURATION_INCLUDED
#define BOARD_CONFIGURATION_INCLUDED

#include "impedanceutilitieslib/include/Configuration.h"
#include "ad5940lib/ad5940.h"

extern MessageByteType const kPreamble[PREAMBLE_SIZE];

int32_t convertInt32ToInt18(int32_t value);
void convertInt32ArrayToInt18Array(int32_t* const data, uint32_t size);
void sendStatusMessage(enum EBoardStatus status);

#endif