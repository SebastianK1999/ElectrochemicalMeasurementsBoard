#ifndef APP_PARAMETERS_INCLUDED
#define APP_PARAMETERS_INCLUDED

#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <devicetree/gpio.h>
#include <stdbool.h>
#include <stdint.h>
#include "ad5940lib/ad5940.h"

#define SEQUENCER_SINGLE_INSTRUCTION_SIZE 4
#define APPS_AD5940_IO_BUFFER_SIZE 1024 
#define SEQUENCE_MEMORY_SIZE SEQMEMSIZE_4KB
extern uint32_t appsAD5940IOBuffer[APPS_AD5940_IO_BUFFER_SIZE];

struct RampConfig
{
    bool        rampBothDirections;
    uint32_t    rampStepNumber;
    float       rampStartMiliVolt;
    float       rampPeakMiliVolt;
    float       rampVzeroStart;
    float       rampVzeroPeak;
    float       rampDuration;
    float       rampSampleDelay;
};

struct AppParameters
{
//  Ad5940 parametrs
    SEQCfg_Type seqencerConfig;
    CLKCfg_Type clockConfig;
    FIFOCfg_Type fifoConfig;
    AGPIOCfg_Type gpioConfig;
    WUPTCfg_Type wakeUpTimerConfig;
    LFOSCMeasure_Type lowFrequencyOscilatorMeasurementCongid;
    
//  App general parameters
    bool appIsRunning;
    SoftSweepCfg_Type sweepConfig;
    SEQInfo_Type initSequenceInfo;
    SEQInfo_Type measureSequenceInfo;

//  ElectrochemicalImpedanceSpectroscopy app paramaters 
    float sweepCurrentFrequency;        // Frequency of measurement, about to be run
    float sweepNextFrequency;           // Frequency of next measurement
    float dataFrequency;                // Frequecy of currently processed data
    float biasVolt;                     // The excitation signal is DC+AC. This parameter decides the DC value in mV unit. 0.0mV means no DC bias.
    float dacVoltagePeakToPeak;         // DAC output voltage in mV peak to peak. Maximum value is 800mVpp. Peak to peak voltage 
    float calibrationResistorValue;     // Calibration resistor value in Ohm 
    float impedanceOutputDataRate;      // The rate of data
    float wakeUpTimerClockFrequency;    // The clock frequency of Wakeup Timer in Hz. Typically it's 32kHz. Leave it here in case we calibrate clock in software method
    float systemClockFrequency;         // The real frequency of system clock
    float adcClockFrequency;            // The real frequency of ADC clock
    uint32_t sequencerStartAddress;     // Initialaztion sequence start address in SRAM of AD5940  
    uint32_t dftNumber;
    uint32_t dftSource;
    uint8_t ADCSinc2Osr;  
    uint8_t ADCSinc3Osr;
    uint8_t adcAverageNumber;           // 2^n poits taken to average
    bool hanningWindowEnabled;


    /* { */ // CVApp 
    struct RampConfig rampConfig;
    /* Common configurations for all kinds of Application. */
    BoolFlag  bParaChanged;         /**< Indicate to generate sequence again. It's auto cleared by AppBIAInit */
    uint32_t  SeqStartAddr;         /**< Initialaztion sequence start address in SRAM of AD5940  */
    uint32_t  MaxSeqLen;            /**< Limit the maximum sequence.   */
    uint32_t  SeqStartAddrCal;      /**< Not used for Ramp.Calibration sequence start address in SRAM of AD5940 */
    uint32_t  MaxSeqLenCal;         /**< Not used for Ramp. */
    /* Application related parameters */ 
    float     LFOSCClkFreq;         /**< The clock frequency of Wakeup Timer in Hz. Typically it's 32kHz. Leave it here in case we calibrate clock in software method */
    float     SysClkFreq;           /**< The real frequency of system clock */
    float     AdcClkFreq;           /**< The real frequency of ADC clock */
    float     RcalVal;              /**< Rcal value in Ohm */
    float     ADCRefVolt;           /**< The real ADC voltage in mV. */
    BoolFlag bTestFinished;			/**< Variable to indicate ramt test has finished >*/

    /* Receive path configuration */
    uint32_t  LPTIARtiaSel;         /**< Select RTIA */
    uint32_t  LPTIARloadSel;				/**< Select Rload */
    float     ExternalRtiaValue;    /**< The optional external RTIA value in Ohm. Disconnect internal RTIA to use external RTIA. When using internal RTIA, this value is ignored. */
    uint32_t  AdcPgaGain;           /**< PGA Gain select from GNPGA_1, GNPGA_1_5, GNPGA_2, GNPGA_4, GNPGA_9 !!! We must ensure signal is in range of +-1.5V which is limited by ADC input stage */   
    //uint8_t   ADCSinc3Osr;          /**< We use data from SINC3 filter. */
    /* Digital related */
    uint32_t  FifoThresh;           /**< FIFO Threshold value */
    /* Private variables for internal usage */
    fImpPol_Type  RtiaValue;        /**< Calibrated Rtia value */
    SEQInfo_Type  InitSeqInfo;
    SEQInfo_Type  ADCSeqInfo;
    BoolFlag      bFirstDACSeq;     /**< Init DAC sequence */
    SEQInfo_Type  DACSeqInfo;       /**< The first DAC update sequence info */
    uint32_t  CurrStepPos;          /**< Current position */     
    float     DACCodePerStep;       /**<  */
    float     CurrRampCode;         /**<  */   
    uint32_t  CurrVzeroCode;        
    BoolFlag  bDACCodeInc;          /**< Increase DAC code.  */
    BoolFlag  StopRequired;         /**< After FIFO is ready, stop the measurement sequence */
    enum _RampState{RAMP_STATE0 = 0, RAMP_STATE1, RAMP_STATE2, RAMP_STATE3, RAMP_STATE4, RAMP_STOP} RampState;


    /* } */
};

void resetBoard();
AD5940Err stopSequence();
void configMCUInteruptFlag();
void clearMCUInteruptFlag();
bool getMCUInteruptFlag();
struct AppParameters* getAppParametersPtr();

#endif