#ifndef COMMANDS_INCLUDED
#define COMMANDS_INCLUDED

void executeConfigureRampCommand(void* message);
void executeConfigureSweepCommand(void* message);
void executePingCommand(void* message);
void executeRunCVAppCommand(void* message);
void executeRunEISAppCommand(void* message);

#endif