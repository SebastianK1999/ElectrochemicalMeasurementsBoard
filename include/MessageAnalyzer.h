#ifndef MESSAGE_ANALYZER_INCLUDED
#define MESSAGE_ANALYZER_INCLUDED

#include <stddef.h>

void analyseMessage(void* buffer, const size_t length);

#endif