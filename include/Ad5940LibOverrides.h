#ifndef AD5940LIB_OVERRIDES_INCLUDED
#define AD5940LIB_OVERRIDES_INCLUDED

#include <stdint.h>
#include "ad5940lib/ad5940.h"

void AD5940_CsClr(void);
void AD5940_CsSet(void);
void AD5940_RstClr(void);
void AD5940_RstSet(void);
void AD5940_Delay10us(uint32_t time);
void AD5940_ReadWriteNBytes(unsigned char *pSendBuffer,unsigned char *pRecvBuff,unsigned long length);
void overriden_AD5940_SweepNext(SoftSweepCfg_Type *pSweepCfg, float *pNextFreq);

#endif