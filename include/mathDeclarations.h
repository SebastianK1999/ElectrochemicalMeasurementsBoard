#ifndef MATH_DECLARATIONS_INCLUDED
#define MATH_DECLARATIONS_INCLUDED

double sqrt(double);
double pow(double, double);
double sin(double);
double cos(double);
double atan2(double, double);
double log10(double);
int abs(int);
long labs(long);
float fabs(float);
double dabs(double);

#endif