#ifndef MESSAGE_SENDER_INCLUDED
#define MESSAGE_SENDER_INCLUDED

#include <stddef.h>
#include <stdbool.h>
#include "impedanceutilitieslib/include/Configuration.h"

void sendMessage(enum EBoardToPcMessage messageType, const void * data, const size_t length);
 
#endif