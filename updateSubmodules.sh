#!/bin/bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
git -C $SCRIPT_DIR submodule update --merge --recursive --remote

#cp $SCRIPT_DIR/externallib/impedanceutilitieslib/include/*.h $SCRIPT_DIR/include/
#cp $SCRIPT_DIR/externallib/impedanceutilitieslib/src/*.c $SCRIPT_DIR/src/
#
#cp $SCRIPT_DIR/externallib/ad5940lib/*.h $SCRIPT_DIR/include/
#cp $SCRIPT_DIR/externallib/ad5940lib/*.c $SCRIPT_DIR/src/